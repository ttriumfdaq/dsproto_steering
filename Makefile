
IMAGE_VERSION := 1.0
DOCKER_IMAGE := arduino-esp32:$(IMAGE_VERSION)
DOCKER_PROJECT_VOLUME := steering
PORT := /dev/ttyUSB0

DOCKER_ARGS := \
	--rm \
	--init \
	-u $(id -u ${USER}):$(id -g ${USER}) \
	-v ${PWD}/steering:/$(DOCKER_PROJECT_VOLUME):rw \
	-it ${DOCKER_IMAGE} bash -l

DOCKER_DEV_ARGS := \
	--privileged \
	-v /dev:/dev:rw

# Check that the docker image exists before continuing
$(shell docker image inspect ${DOCKER_IMAGE} 2>&1 > /dev/null)
ifneq ($(.SHELLSTATUS), 0)
$(error "Please run 'make docker' to create required docker image")
endif

.PHONY: help docker interactive build upload terminal

default: help

help: ## List available targets
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

docker: ## Build docker image
	@docker build -t ${DOCKER_IMAGE} docker

interactive: ## Interactive mode inside docker image
	@docker run $(DOCKER_ARGS)

build: ## Build Arduino binary	
	@docker run $(DOCKER_ARGS) -c "arduino-cli compile -e --fqbn esp32:esp32:featheresp32 steering"
	
upload: ## Upload binary to ESP32 featherwing
	@docker run $(DOCKER_DEV_ARGS) $(DOCKER_ARGS) -c "arduino-cli upload -p $(PORT) --input-dir steering/build/esp32.esp32.featheresp32 --fqbn esp32:esp32:featheresp32 steering"

terminal: ## Open up a terminal to the ESP32
	@echo "Hit CTRL+a, CTRL+x to quit picocom"
	@docker run $(DOCKER_DEV_ARGS) $(DOCKER_ARGS) -c "picocom -q -b 115200 $(PORT)"

