//Eric Raguzin
//Brookhaven National Laboratory
//Control for the IO-1733-1 PCB serial chain for Darkside test with LTC6820 interface and FeatherWing Ethernet shield
//8/14/19
#include <WiFi.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <EEPROM.h>
#include <CRC32.h>

#define USE_DHCP 1 // comment this out to use a static IP
#define DHCP_RETRY_DELAY_IN_MS 2000

//LTC6820 must be in the POL=0, PHA=1 positions.  Full explanation at bottom of file

//Adjust speed of SPI write (to compensate for any issues in cryo)/////////
#define SPI_DELAY_IN_US 100

////////////////////////////////////////////////////////////////////////////
//To translate to LTC6820 language, MOSI is D_IN and DATA, MISO is D_OUT and READBACK
#define D_IN  12  // Data out
#define D_OUT 13  // Readback coming in
#define LE    15  // Latch Enable
#define CLK   27  // SCLK

#define ETHERNET_CS_PIN 33
#define BITS 96
#define PACKED_BITS (BITS/4) // packed bits into hexadecimal characters, 24 * 4 = 96
#define CMD_SIZE (1)
#define CRC_SIZE (4)
#define EEPROM_SIZE (PACKED_BITS + CRC_SIZE)
#define UDP_PACKET_SIZE (CMD_SIZE + PACKED_BITS + CRC_SIZE) // TYPE(CHAR) + PACKED BITS + CRC32 (uint32_t)

#define PORT 32000      // local port to listen on

// Todo low voltage and high voltage is not correct

// It's organized so that the first 4 arrays are for the low voltage amplifier bias, the last 8 are for the high voltage SiPM bias
// When printed, the arrays go from Channel 1 on the left to Channel 32 or 64 on the right
//  This defaults to all outputs NOT allowing high voltage (the two chips require opposite polarities for that).

// All off
// byte array_to_set[12] = {255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0};
// All on
//  byte array_to_set[12] = {0,0,0,0,255,255,255,255,255,255,255,255};
// byte array_readback[12] = {0};

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
uint8_t mac_address[6];

IPAddress ip(192, 168, 1, 200);

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

// Handy buffers
char array_to_set[BITS];
char array_to_read[BITS];

char array_eeprom[EEPROM_SIZE];

bool debug = true;
bool ethernet_alive = false;
bool ethernet_connected = false;

void setup() {
  // Setup SPI pins
  pinMode(D_IN, OUTPUT);
  pinMode(D_OUT, INPUT);
  pinMode(LE, OUTPUT);
  pinMode(CLK, OUTPUT);

  //Latch off
  //POL 0, PHA 0 requires CLK to idle low
  digitalWrite(LE, HIGH);
  digitalWrite(CLK, LOW);
  digitalWrite(D_IN, LOW);

  // Open serial communications
  Serial.begin(115200);

  Serial.print("Built: ");
  Serial.println(__TIMESTAMP__);

  // Initialize and open EEPROM
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.get(0, array_eeprom);

  // Check if EEPROM is valid, if not, write defaults
  if(CRC32::calculate(array_eeprom, PACKED_BITS) != *(uint32_t*)&array_eeprom[PACKED_BITS]) {
    Serial.println("EEPROM Invalid, using default values: ");
    //Writes the initial array to the chips so that all outputs are off.  It has a habit of starting up in a state that allows HV through.
    for(int i=(BITS-1); i > -1; i--) {
      array_to_set[i] = (i > 63) ? 1 : 0;
      Serial.print(array_to_set[i]);
    }
    Serial.println();
    convert_array_to_msg(array_to_set, array_eeprom);
    *(uint32_t*)&array_eeprom[PACKED_BITS] = CRC32::calculate(array_eeprom, PACKED_BITS); 
    EEPROM.put(0, array_eeprom);
    EEPROM.commit();
  } else {
    Serial.println("Successfully loaded initial values from EEPROM");
    convert_msg_to_array(array_eeprom, array_to_set);
  }

  array_eeprom[PACKED_BITS] = '\0';

  Serial.print("Initial values: ");
  Serial.print(array_eeprom);  
  Serial.println();

  write_chip(array_to_set, array_to_read);

  WiFi.macAddress(mac_address);
  Serial.print("MAC address: ");
  for (byte octet = 0; octet < 6; octet++) {
    Serial.print(mac_address[octet], HEX);
    if (octet < 5) {
      Serial.print(':');
    }
  }
  Serial.println();

  // You can use Ethernet.init(pin) to configure the CS pin
  Ethernet.init(ETHERNET_CS_PIN);  // Most Arduino shields

  // start the Ethernet
  if(USE_DHCP) {  
    ethernet_alive = Ethernet.begin(mac_address);
  } else {
    Ethernet.begin(mac_address, ip);
    ethernet_alive = true;
  }
  
  // Check for Ethernet hardware present
  if(Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found. Please attach");
    while (Ethernet.hardwareStatus() == EthernetNoHardware) {  
      delay(1); // do nothing, no point running without Ethernet hardware    
    }
  }

  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  } else {
    ethernet_connected = true;
  }

  if(ethernet_alive) {
    Serial.print("Listening on ");
    Serial.print(Ethernet.localIP());
    Serial.print(":");
    Serial.println(PORT);
  } else {
    Serial.println("Failed to get IP address");
  }

  // start UDP
  Udp.begin(PORT);
}

void loop() {
  static char rxBuffer[UDP_PACKET_SIZE+1];
  static char txBuffer[UDP_PACKET_SIZE+1];
  uint16_t bytesToSend;

  if((ethernet_connected) && (Ethernet.linkStatus() == LinkOFF)) {
    Serial.println("Ethernet cable is not connected.");
    ethernet_connected = false;
  } 
  
  if((!ethernet_connected) && (Ethernet.linkStatus() == LinkON)) {
    Serial.println("Ethernet cable is connected.");
    ethernet_connected = true;
  }

  if(USE_DHCP) {
    byte result = Ethernet.maintain();
    if((!ethernet_alive) && ((result == 2) || (result == 4))) {
      ethernet_alive = true;
      if(ethernet_alive) {
        Serial.print("Listening on ");
        Serial.print(Ethernet.localIP());
        Serial.print(":");
        Serial.println(PORT);
      }
    }
  }

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();  
  if (packetSize == UDP_PACKET_SIZE) {
    
    // Get IP and PORT
    IPAddress remote_ip = Udp.remoteIP();
    uint16_t remote_port = Udp.remotePort();

    packetSize = Udp.read(rxBuffer, UDP_PACKET_SIZE);
    rxBuffer[packetSize] = '\0'; // safe because rxBuffer is UDP_PACKET_SIZE+1, and packetSize is max UDP_PACKET_SIZE
    if(debug) {
      Serial.print("Received packet of size ");
      Serial.print(packetSize);
      Serial.print(" from ");
      Serial.print(remote_ip);
      Serial.print(":");
      Serial.println(remote_port);
      Serial.print("RX: ");
      Serial.println(rxBuffer);
    }

    // Validate CRC32 and fail silently if it doesn't work ( security thru obscurity :) )
    if(*((uint32_t*)&rxBuffer[PACKED_BITS+1]) != CRC32::calculate(rxBuffer, PACKED_BITS+1)) {
      if(debug) {
        Serial.println("Invalid CRC32 received");
        Serial.println(*(uint32_t*)&rxBuffer[PACKED_BITS+1]);
        Serial.println(CRC32::calculate(rxBuffer, PACKED_BITS+1));
      }
      return;
    }

    // default to error message
    txBuffer[0] = 'E';
    for(int i=0; i<PACKED_BITS; i++) {
      txBuffer[i+1] = '0';
    }    

    switch(rxBuffer[0]) {
      case 'W': // write               
        // Message to small to be valid
        if(packetSize != UDP_PACKET_SIZE) {          
          break;
        }

        // Read in chip values, and convert them from ascii '0','1' to 0,1
        if(convert_msg_to_array(&rxBuffer[1], array_to_set)) {
          write_chip(array_to_set, array_to_read);
          convert_array_to_msg(array_to_set, &txBuffer[1]);
          txBuffer[0] = 'S';
        } else if(debug) {
            Serial.print("Error: Invalid array sent");          
        }
        break;

      case 'R': // readback
        // rewrite the last given values to the chip and get the response
        write_chip(array_to_set, array_to_read);        
        convert_array_to_msg(array_to_read, &txBuffer[1]);
        txBuffer[0] = 'S';
        break;

      case 'S': // save
        // save array to set to EEPROM
        convert_array_to_msg(array_to_set, array_eeprom);
        *(uint32_t*)&array_eeprom[PACKED_BITS] = CRC32::calculate(array_eeprom, PACKED_BITS); 
        EEPROM.put(0, array_eeprom);
        EEPROM.commit();
        convert_array_to_msg(array_to_set, &txBuffer[1]);
        txBuffer[0] = 'S';
        break;

      case 'L': // load
        // load array to set from EEPROM and then write it to chip
        EEPROM.get(0, array_eeprom);
        convert_msg_to_array(array_eeprom, array_to_set);
        write_chip(array_to_set, array_to_read);        
        convert_array_to_msg(array_to_set, &txBuffer[1]);
        txBuffer[0] = 'S';
        break;

      case 'D': // debug change
        debug = (rxBuffer[1] == '1') ? true : false;
        txBuffer[0] = 'S';
        break;

      case 'B': // build info
        sprintf(txBuffer, "%c%s", 'S', __TIMESTAMP__);
        break;

      default: // error
        if(debug) {
          Serial.print("Error: Function indicator not correct. Was ");
          Serial.println(rxBuffer[0]);
        }
        break;
    }

    *(uint32_t*)&txBuffer[PACKED_BITS+1] = CRC32::calculate(txBuffer, PACKED_BITS+1); 
    bytesToSend = UDP_PACKET_SIZE; // ERROR type + PACKED BITS + CRC32

    if(debug) {
      txBuffer[bytesToSend] = '\0'; // bytesToSend shall never exceed UDP_PACKET_SIZE, txBuffer is defined as UDP_PACKET_SIZE+1
      Serial.print("TX: ");
      Serial.println(txBuffer);      
    }

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(remote_ip, remote_port);
    Udp.write(txBuffer, bytesToSend);
    Udp.endPacket();  
  }

  delay(10);
}

bool convert_msg_to_array(const char* msg, char* array) {  
  uint8_t value;
  uint8_t offset;

  // Validate the message before we write to the array
  for (int i = 0; i < PACKED_BITS; i++) {    
    if(!((msg[i] >= '0') && (msg[i] <= '9') || (msg[i] >= 'A') && (msg[i] <= 'F'))) {
      return false;
    }
  }

  // Set the array based on the message
  for (int i = 0; i < PACKED_BITS; i++) {
    if((msg[i] >= '0') && (msg[i] <= '9')) {
      value = msg[i] - '0';
    } else {
      value = (msg[i] - 'A') + 10;
    }

    for(int n=0; n<4; n++) {
      array[(i*4)+n] = (value & (1 << n)) ? 1 : 0;
    }    
  }

  return true;
}
     
void convert_array_to_msg(const char* array, char* msg) {
  uint8_t value;

  for (int i = 0; i < PACKED_BITS; i++) {

    value = (array[(i*4)+0] << 0) |
      (array[(i*4)+1] << 1) |
      (array[(i*4)+2] << 2) |
      (array[(i*4)+3] << 3);

    msg[i] = (value < 10) ? '0' + value : 'A' + (value - 10);
  }
}

bool write_chip(const char* write_values, char* read_values) {
  //Cycles through this twice.  The first time shifts the settings in the latches, and the second time, the previous settings are outputted, so you can read and compare
  //This cycles through the 96 bits in the correct order.  For each bit, it is "clocked" in, and the output bit is read.
  //The HV 5523 and HV 3418 latch in the data on different clock edges, so the transient analysis in complicated, especially with the LTC6820 between them, but this was found experimentally to work
  //Because of the register orientation, you want to put the last bit first
  //With the LTC6820, a change of LE will send that across to the slave module
  //With PHA = 0, POL = 0, a rising clock edge will sample D_IN and send that data to the slave module.  The slave module will adjust MOSI and then do a quick positive/negative clock pulse
  //and then sample the readback on MISO and send that back.
  //HOWEVER, the Master only adjusts its MISO (sampled as D_OUT here) AFTER the Arduino does the falling clock edge.  Thanks Linear for hiding that information in a figure.

  //Wake up the sleeping module
  digitalWrite(LE, LOW);
  digitalWrite(LE, HIGH);

  for (int m = 0; m < 2; m++) {
    digitalWrite(LE, LOW);
    // Write in MSB first
    for (int i = (BITS-1); i > -1; i--) {
      read_values[i] = digitalRead(D_OUT);
      digitalWrite(D_IN, write_values[i]);
      delayMicroseconds(SPI_DELAY_IN_US);

      digitalWrite(CLK, HIGH);
      delayMicroseconds(SPI_DELAY_IN_US);

      digitalWrite(CLK, LOW);
      delayMicroseconds(SPI_DELAY_IN_US);

      if (i == 0) {
        digitalWrite(LE, HIGH);
        delayMicroseconds(SPI_DELAY_IN_US);
        digitalWrite(LE, LOW);
        delayMicroseconds(SPI_DELAY_IN_US);
        digitalWrite(LE, HIGH);
        delayMicroseconds(SPI_DELAY_IN_US);
        digitalWrite(D_IN, LOW);
      }
    }
  }

  //Check to make sure the chip was written correctly
  for(int i = 0; i < BITS; i++) {
    if (write_values[i] != read_values[i]) {
      return false;
    }
  }

  return true;
}

//Configuration explanation
//The IO-1733-1 board is designed with the SPI chain below with shared clock/LE line:

//input -> HV5523 -> HV3418 -> readback

//If you look at the data sheets, you'll see that the HV3418 shifts new bits in on a CLK rising edge and latches it (enables the output) on a LE falling edge
//while the HV5523 shifts new bits in on a CLK falling edge and latches on a LE rising edge.  I know, less than optimal, but these were the only chips that fit what we needed and worked at cryo.

//We have 4 options for the POL and PHA pins of the LTC6820.
//The datasheet shows that the SCK pulse lasts for 100 ns or 1us depending on the SLOW pin.  Because we're going to be in cryo, I'd rather use SLOW to give us the margin if we need it, so we make sure the MOSI pin has settled.
//At room temp, the HV3418 requires the data line to be stable for ~30 ns before and after the clock, which must be >83 ns.  The output reflects the bit that you pushed 160 ns after the clock pulses.  The timing requirements are a little less
//strict for the HV5523, but still, because everything could completely change in cryo, it's better being safe.  For that reason POL=0, PHA=1 and POL=1, PHA=0 are out because I don't want a falling/rising SCK pulse.
//That pattern would possibly create a race condition by shifting the HV5523 and immediately shifting it into the HV3418.  Because we have no control over the SCK pulse width, I'd much rather have a rising/falling SCK pulse.

//Now, with those options, POL=1, PHA=1 is more tricky to deal with.  The CS (or LE in our case) pin initiating the write also triggers SCK to fall and then rise at the end when LE comes back up.
//This shifts bits into either chip when we don't have control over the SPI data line.  It also makes things annoying at the end, where we need both LE edges to get both chips to latch, but in this case, that would actually keep shifting bits in.
//It would be just plain annoying to deal with all that.

//With POL=0, PHA=0, there are no suprise SCK edges from CS/LE, so I can just pulse those as much as I want to latch both chips.  The only downside is that the readback is shifted by one bit.  Because the first rising edge of the SCK pulse train
//Causes the HV3418 to shift in the last bit sent from the HV5523, the HV3418 that outputs the readback is one bit "behind".  I make up for this by having the random SCK pulse after the LE goes low in the write_chip() function
