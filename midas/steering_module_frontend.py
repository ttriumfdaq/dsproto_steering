"""
Frontend for controlling the Darkside Proto-0 steering module.
"""

import midas
import copy
import midas.frontend
import collections
import steering_module_driver

class FakeSteeringModuleDriver(steering_module_driver.SteeringModule):
    """
    While this frontend was being developed, we weren't able to use the real
    steering module. This dummy class defines a "driver" that doesn't actually
    try to communicate with the arduino, and just assumes that the update
    worked successfully.
    """
    def write_Arduino_BNL(self, function):
        self.handle_readback(self.write_array)

    def write_Arduino_TRIUMF(self, function):
        self.handle_readback(self.write_array)

class SteeringModuleEquipment(midas.frontend.EquipmentBase):
    """
    Periodic equipment for controlling the steering module. We don't write to
    banks/history, but just update a /Readback section in the ODB.
    
    Steering module updates are rather slow, so instead of pushing an update
    every time the ODB changes, we batch them up and do them at most once every
    period (default 100ms). This should make things more responsive for users
    in the long-run.

    Arguments:
        * client (`midas.client.MidasClient`)
        * use_fake_device (bool) - Don't connect to a real Steering Module,
            just assume that all changes were sent correctly.
        * equip_name (string) - What to call the Equipment we create.
    
    Members:
        * driver (`steering_module_driver.SteeringModule`)
        * readback (dict) - Current state of the user-facing channels.
        * prev_readback (dict) - What `readback` was last time we read it.
        * odb_readback_dir (str) - Where we'll store the readback values.
        * have_update (bool) - Whether there are channel changes to apply.
    """
    def __init__(self, midas_client, use_fake_device, equip_name):
        default_common = midas.frontend.InitialEquipmentCommon()
        default_common.equip_type = midas.EQ_PERIODIC
        default_common.read_when = midas.RO_ALWAYS
        default_common.event_id = 57
        default_common.period_ms = 100
        
        # These defaults are only written the first time frontend starts
        default_settings = collections.OrderedDict([
                            ("Read only", False),
                            ("Use BNL protocol", True),
                            ("Use PDUPlus map", False),
                            ("Original map", collections.OrderedDict([
                                ("uC regulator", False),
                                ("Low voltage", [False] * 25),
                                ("High voltage", [False] * 25),
                                ("Real channels", collections.OrderedDict([
                                    ("uC regulator", 1),
                                    ("Amp regulator", 2),
                                    ("Low voltage", [16,15,14,13,12,11,10,9,8,7,6,5,4,3,22,23,24,25,26,27,28,29,30,31,32]),
                                    ("High voltage", [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25])
                                    ])
                                ),
                                ])
                            ),
                            ("PDUPlus map", collections.OrderedDict([
                                ("Quadrants", [False] * 4),
                                ("Quadrant 1 tiles LV", [False] * 4),
                                ("Quadrant 2 tiles LV", [False] * 4),
                                ("Quadrant 3 tiles LV", [False] * 4),
                                ("Quadrant 4 tiles LV", [False] * 4),
                                ("Quadrant 1 tiles HV", [False] * 4),
                                ("Quadrant 2 tiles HV", [False] * 4),
                                ("Quadrant 3 tiles HV", [False] * 4),
                                ("Quadrant 4 tiles HV", [False] * 4),
                                ("Real channels", collections.OrderedDict([
                                    ("Quadrants", [11,16,21,26]),
                                    ("Quadrant 1 tiles LV", [12,13,14,15]),
                                    ("Quadrant 2 tiles LV", [17,18,19,20]),
                                    ("Quadrant 3 tiles LV", [22,23,24,25]),
                                    ("Quadrant 4 tiles LV", [27,28,29,30]),
                                    ("Quadrant 1 tiles HV", [1,2,3,4]),
                                    ("Quadrant 2 tiles HV", [5,6,7,8]),
                                    ("Quadrant 3 tiles HV", [9,10,11,12]),
                                    ("Quadrant 4 tiles HV", [13,14,15,16])
                                    ])
                                )
                                ])
                            ),
                            ("Network settings BNL", collections.OrderedDict([
                                ("Arduino IP", "192.168.121.2"),
                                ("Arduino port", 32000),
                                ("Recv IP", "192.168.121.1"),
                                ("Recv port", 32001),
                                ("Recv buffer size", 104),
                                ("Socket timeout (secs)", 3.0)
                                ])
                            ),
                            ("Network settings TRIUMF", collections.OrderedDict([
                                ("Arduino IP", "192.168.121.2"),
                                ("Arduino port", 32000),
                                ("Recv buffer size", 96),
                                ("Socket timeout (secs)", 3.0)
                                ])
                            )
                        ])
        
        midas.frontend.EquipmentBase.__init__(self, midas_client, equip_name, default_common, default_settings)
    
        use_bnl, network_settings = self.get_current_network_settings()

        if use_fake_device:
            self.driver = FakeSteeringModuleDriver(use_bnl, network_settings, self.settings["Use PDUPlus map"])
        else:
            self.driver = steering_module_driver.SteeringModule(use_bnl, network_settings, self.settings["Use PDUPlus map"])

        self.prev_settings = default_settings.copy()
        
        self.readback = {
            "Original map": collections.OrderedDict([("uC regulator", False),
                                                     ("Low voltage", [False] * 25),
                                                     ("High voltage", [False] * 25)]),
            "PDUPlus map": collections.OrderedDict([
                                    ("Quadrants", [False] * 4),
                                    ("Quadrant 1 tiles LV", [False] * 4),
                                    ("Quadrant 2 tiles LV", [False] * 4),
                                    ("Quadrant 3 tiles LV", [False] * 4),
                                    ("Quadrant 4 tiles LV", [False] * 4),
                                    ("Quadrant 1 tiles HV", [False] * 4),
                                    ("Quadrant 2 tiles HV", [False] * 4),
                                    ("Quadrant 3 tiles HV", [False] * 4),
                                    ("Quadrant 4 tiles HV", [False] * 4)
            ])
        }
                                                 
        self.prev_readback = copy.deepcopy(self.readback)
        self.odb_readback_dir = self.odb_settings_dir.replace("Settings", "Readback")
        self.client.odb_set(self.odb_readback_dir, self.readback)

        # Tell the driver about our channel map
        self.validate_channel_map()
        self.force_write_to_driver = True
        self.settings_changed_func()
        self.force_write_to_driver = False

        # The SM has no way to just "read" the current state. We must push our
        # ODB settings to the board to ensure Settings and Readback are correct.
        self.readout_func()

    def get_current_network_settings(self):
        """
        Read the ODB settings, and extract the network-related parameters that
        the driver needs to know about.
        
        Returns:
            2-tuple of (bool, network settings)
            `steering_module_driver.NetworkSettingsBNL` or 
            `steering_module_driver.NetworkSettingsTRIUMF`
        """
        if self.settings["Use BNL protocol"]:
            odb_bnl = self.settings["Network settings BNL"]
            network = steering_module_driver.NetworkSettingsBNL()
            network.arduino_ip = odb_bnl["Arduino IP"]
            network.arduino_port = odb_bnl["Arduino port"]
            network.our_ip = odb_bnl["Recv IP"]
            network.recv_buffer_size = odb_bnl["Recv buffer size"]
            network.recv_port = odb_bnl["Recv port"]
            network.socket_timeout_secs = odb_bnl["Socket timeout (secs)"]
        else:
            odb_tri = self.settings["Network settings TRIUMF"]
            network = steering_module_driver.NetworkSettingsTRIUMF()
            network.arduino_ip = odb_tri["Arduino IP"]
            network.arduino_port = odb_tri["Arduino port"]
            network.recv_buffer_size = odb_tri["Recv buffer size"]
            network.socket_timeout_secs = odb_tri["Socket timeout (secs)"]

        return (self.settings["Use BNL protocol"], network)

    def validate_channel_map(self):
        """
        Validate that the user hasn't set the same channel multiple times.
        """
        if not self.settings["Use PDUPlus map"]:
            uc_idx = self.settings["Original map"]["Real channels"]["uC regulator"]
            amp_idx = self.settings["Original map"]["Real channels"]["Amp regulator"]
            lv_idxs = self.settings["Original map"]["Real channels"]["Low voltage"]
            hv_idxs = [x + self.driver.hv_offset for x in self.settings["Original map"]["Real channels"]["High voltage"]]
            
            all_idxs = [uc_idx, amp_idx]
            all_idxs.extend(lv_idxs)
            all_idxs.extend(hv_idxs)
            all_idxs = sorted(all_idxs)

            self.driver.set_real_channel_uc(uc_idx)
            self.driver.set_real_channels_lv(lv_idxs)
            self.driver.set_real_channels_hv(hv_idxs)
        else:
            q_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrants"]
            q1_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 1 tiles LV"]
            q2_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 2 tiles LV"]
            q3_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 3 tiles LV"]
            q4_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 4 tiles LV"]
            q1_hv_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 1 tiles HV"]
            q2_hv_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 2 tiles HV"]
            q3_hv_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 3 tiles HV"]
            q4_hv_idxs = self.settings["PDUPlus map"]["Real channels"]["Quadrant 4 tiles HV"]
            
            all_idxs = []
            all_idxs.extend(q_idxs)
            all_idxs.extend(q1_idxs)
            all_idxs.extend(q2_idxs)
            all_idxs.extend(q3_idxs)
            all_idxs.extend(q4_idxs)
            all_idxs.extend(q1_hv_idxs)
            all_idxs.extend(q2_hv_idxs)
            all_idxs.extend(q3_hv_idxs)
            all_idxs.extend(q4_hv_idxs)

            self.driver.set_real_channels_quadrants(q_idxs)
            self.driver.set_real_channels_quadrant_tiles(1, q1_idxs, q1_hv_idxs)
            self.driver.set_real_channels_quadrant_tiles(2, q2_idxs, q2_hv_idxs)
            self.driver.set_real_channels_quadrant_tiles(3, q3_idxs, q3_hv_idxs)
            self.driver.set_real_channels_quadrant_tiles(4, q4_idxs, q4_hv_idxs)

        for i in range(1, len(all_idxs)):
            if all_idxs[i] == all_idxs[i-1]:
                raise ValueError("Invalid channel mapping - %s appears more than once" % all_idxs[i])
            
    def readout_func(self):
        """
        This is called automatically by the frontend framework periodically.
        
        We tell the driver about any channels that need turning on/off, and
        update the ODB with any readback changes that have happened.
        """
        if self.have_updates:
            if self.settings["Read only"]:
                self.client.msg("SteeringModule is in read-only mode - will not change which channels are on/off")
            else:
                self.driver.write_updates()
                
            self.have_updates = False
        
        if not self.settings["Use PDUPlus map"]:
            self.readback["Original map"]["uC regulator"] = self.driver.rdb_orig_uc()
            self.readback["Original map"]["Low voltage"] = self.driver.rdb_orig_lv()
            self.readback["Original map"]["High voltage"] = self.driver.rdb_orig_hv()
        else:
            self.readback["PDUPlus map"]["Quadrants"] = self.driver.rdb_quadrants()
            self.readback["PDUPlus map"]["Quadrant 1 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(1)
            self.readback["PDUPlus map"]["Quadrant 2 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(2)
            self.readback["PDUPlus map"]["Quadrant 3 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(3)
            self.readback["PDUPlus map"]["Quadrant 4 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(4)
            self.readback["PDUPlus map"]["Quadrant 1 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(1)
            self.readback["PDUPlus map"]["Quadrant 2 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(2)
            self.readback["PDUPlus map"]["Quadrant 3 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(3)
            self.readback["PDUPlus map"]["Quadrant 4 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(4)
            
        if self.prev_readback != self.readback:
            self.client.odb_set(self.odb_readback_dir, self.readback)
            
        self.prev_settings = copy.deepcopy(self.settings)
        self.prev_readback = copy.deepcopy(self.readback)
        return None
    
    def write_readback_to_settings(self):
        """
        Copy the content of the readback to the settings, and update in the ODB.
        """
        if not self.settings["Use PDUPlus map"]:
            self.settings["Original map"]["uC regulator"] = self.readback["Original map"]["uC regulator"]
            self.settings["Original map"]["Low voltage"] = self.readback["Original map"]["Low voltage"]
            self.settings["Original map"]["High voltage"] = self.readback["Original map"]["High voltage"]
        else:
            self.settings["PDUPlus map"]["Quadrants"] = self.readback["PDUPlus map"]["Quadrants"]
            self.settings["PDUPlus map"]["Quadrant 1 tiles LV"] = self.readback["PDUPlus map"]["Quadrant 1 tiles LV"]
            self.settings["PDUPlus map"]["Quadrant 2 tiles LV"] = self.readback["PDUPlus map"]["Quadrant 2 tiles LV"]
            self.settings["PDUPlus map"]["Quadrant 3 tiles LV"] = self.readback["PDUPlus map"]["Quadrant 3 tiles LV"]
            self.settings["PDUPlus map"]["Quadrant 4 tiles LV"] = self.readback["PDUPlus map"]["Quadrant 4 tiles LV"]
            self.settings["PDUPlus map"]["Quadrant 1 tiles HV"] = self.readback["PDUPlus map"]["Quadrant 1 tiles HV"]
            self.settings["PDUPlus map"]["Quadrant 2 tiles HV"] = self.readback["PDUPlus map"]["Quadrant 2 tiles HV"]
            self.settings["PDUPlus map"]["Quadrant 3 tiles HV"] = self.readback["PDUPlus map"]["Quadrant 3 tiles HV"]
            self.settings["PDUPlus map"]["Quadrant 4 tiles HV"] = self.readback["PDUPlus map"]["Quadrant 4 tiles HV"]

        self.prev_settings = self.settings.copy()
        self.client.odb_set(self.odb_settings_dir, self.settings, remove_unspecified_keys=False)
    
    def settings_changed_func(self):
        """
        See note in class documentation about why we don't tell the driver
        immediately about any changes to which channels should be on/off.
        
        We just update the driver's network settings if needed.
        """

        if self.settings["PDUPlus map"]["Real channels"] != self.prev_settings["PDUPlus map"]["Real channels"]:
            self.client.msg("Restart the frontend to pick up the changed channel configuration", True)

        if self.settings["Original map"]["Real channels"] != self.prev_settings["Original map"]["Real channels"]:
            self.client.msg("Restart the frontend to pick up the changed channel configuration", True)
                
        use_bnl, network_settings = self.get_current_network_settings()
        self.driver.bnl_mode = use_bnl
        self.driver.network = network_settings
    
        if not self.settings["Use PDUPlus map"]:
            if self.force_write_to_driver or self.prev_settings["Original map"]["uC regulator"] != self.settings["Original map"]["uC regulator"]:
                self.driver.set_uc(self.settings["Original map"]["uC regulator"], immediate_commit=False)
                self.have_updates = True
                    
            if self.force_write_to_driver or self.prev_settings["Original map"]["Low voltage"] != self.settings["Original map"]["Low voltage"]:
                self.driver.set_orig_lv(self.settings["Original map"]["Low voltage"], immediate_commit=False)
                self.have_updates = True
                
            if self.force_write_to_driver or self.prev_settings["Original map"]["High voltage"] != self.settings["Original map"]["High voltage"]:
                self.driver.set_orig_hv(self.settings["Original map"]["High voltage"], immediate_commit=False)
                self.have_updates = True
        else:
            if self.force_write_to_driver or self.prev_settings["PDUPlus map"]["Quadrants"] != self.settings["PDUPlus map"]["Quadrants"]:
                self.driver.set_quadrants(self.settings["PDUPlus map"]["Quadrants"], immediate_commit=False)
                self.have_updates = True

            for q in range(1,5):
                key_lv = "Quadrant %s tiles LV" % q
                key_hv = "Quadrant %s tiles HV" % q

                if self.force_write_to_driver or self.prev_settings["PDUPlus map"][key_lv] != self.settings["PDUPlus map"][key_lv]:
                    self.driver.set_quadrant_tiles_lv(q, self.settings["PDUPlus map"][key_lv], immediate_commit=False)
                    self.have_updates = True

                if self.force_write_to_driver or self.prev_settings["PDUPlus map"][key_hv] != self.settings["PDUPlus map"][key_hv]:
                    self.driver.set_quadrant_tiles_hv(q, self.settings["PDUPlus map"][key_hv], immediate_commit=False)
                    self.have_updates = True
    
class SteeringModuleFrontend(midas.frontend.FrontendBase):
    """
    Frontend class - we only have 1 equipment to control.
    """
    def __init__(self, use_fake_device, sm_name):
        prog_name = "SteeringModule%s" % sm_name
        midas.frontend.FrontendBase.__init__(self, prog_name)
        self.add_equipment(SteeringModuleEquipment(self.client, use_fake_device, prog_name))
    
if __name__ == "__main__":
    parser = midas.frontend.parser
    parser.add_argument("--use-fake-device", action="store_true")
    parser.add_argument("--sm-name", default="")
    args = midas.frontend.parse_args()

    fe = SteeringModuleFrontend(args.use_fake_device, args.sm_name)
    fe.run()