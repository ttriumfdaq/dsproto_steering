#!/usr/bin/python3
"""
Frontend for controlling the Darkside Proto-0 control module.
"""

import midas
import copy
import midas.frontend
import collections
import datetime
import control_module_driver

class FakeControlModuleDriver(control_module_driver.ControlModule):
    """
    While this frontend was being developed, we weren't able to use the real
    steering module. This dummy class defines a "driver" that doesn't actually
    try to communicate with the arduino, and just assumes that the update
    worked successfully.
    """
    def __init__(self):
        super().__init__()

    def flush(self):
        return

    def change_usb_port(self, port):
        pass

    def change_ethernet_settings(self, ip, port, recv_buffer_size, timeout_secs):
        pass

    def send_usb_command(self, command, do_readback=True, write_timeout=0.1, read_timeout=0.1):
        pass

    def send_ethernet(self, msg):
        pass

    def write_updates(self, pdu):
        if self.verbose:
            print("Fake device for PDU %d: new LV state: %s" % (pdu, self.pending_lv[pdu]))
            print("Fake device for PDU %d: new HV state: %s" % (pdu, self.pending_hv[pdu]))

        self.rdb[pdu]["LV"] = self.pending_lv[pdu]
        self.rdb[pdu]["HV"] = self.pending_hv[pdu]

class ControlModuleEquipment(midas.frontend.EquipmentBase):
    """
    Periodic equipment for controlling the control module. We don't write to
    banks/history, but just update a /Readback section in the ODB.
    
    Control module updates are rather slow, so instead of pushing an update
    every time the ODB changes, we batch them up and do them at most once every
    period (default 100ms). This should make things more responsive for users
    in the long-run.

    Arguments:
        * client (`midas.client.MidasClient`)
        * use_fake_device (bool) - Don't connect to a real Control Module,
            just assume that all changes were sent correctly.
        * equip_name (string) - What to call the Equipment we create.
        * verbose (bool)
    
    Members:
        * driver (`control_module_driver.ControlModule`)
        * readback (dict) - Current state of the user-facing channels.
        * prev_readback (dict) - What `readback` was last time we read it.
        * odb_readback_dir (str) - Where we'll store the readback values.
        * have_updates (bool[]) - Whether there are channel changes to apply for each PDU.
    """
    def __init__(self, midas_client, use_fake_device, equip_name, verbose):
        default_common = midas.frontend.InitialEquipmentCommon()
        default_common.equip_type = midas.EQ_PERIODIC
        default_common.read_when = midas.RO_ALWAYS
        default_common.event_id = 57
        default_common.period_ms = 100

        pdu_settings = collections.OrderedDict([
                                ("Enable", True),
                                ("Human name", ""),
                                ("Quadrants", [False] * 4),
                                ("Quadrant 1 tiles LV", [False] * 4),
                                ("Quadrant 2 tiles LV", [False] * 4),
                                ("Quadrant 3 tiles LV", [False] * 4),
                                ("Quadrant 4 tiles LV", [False] * 4),
                                ("Quadrant 1 tiles HV", [False] * 4),
                                ("Quadrant 2 tiles HV", [False] * 4),
                                ("Quadrant 3 tiles HV", [False] * 4),
                                ("Quadrant 4 tiles HV", [False] * 4),
                                ("PDUID", False),
                                ])
        
        pdu_readback = collections.OrderedDict([
                                ("Quadrants", [False] * 4),
                                ("Quadrant 1 tiles LV", [False] * 4),
                                ("Quadrant 2 tiles LV", [False] * 4),
                                ("Quadrant 3 tiles LV", [False] * 4),
                                ("Quadrant 4 tiles LV", [False] * 4),
                                ("Quadrant 1 tiles HV", [False] * 4),
                                ("Quadrant 2 tiles HV", [False] * 4),
                                ("Quadrant 3 tiles HV", [False] * 4),
                                ("Quadrant 4 tiles HV", [False] * 4),
                                ("PDUID", False),
                                ])

        # These defaults are only written the first time frontend starts
        default_settings = collections.OrderedDict([
                            ("Read only", False),
                            ("PDU 0", copy.deepcopy(pdu_settings)),
                            ("PDU 1", copy.deepcopy(pdu_settings)),
                            ("PDU 2", copy.deepcopy(pdu_settings)),
                            ("PDU 3", copy.deepcopy(pdu_settings)),
                            ("PDU 4", copy.deepcopy(pdu_settings)),
                            ("PDU 5", copy.deepcopy(pdu_settings)),
                            ("PDU 6", copy.deepcopy(pdu_settings)),
                            ("PDU 7", copy.deepcopy(pdu_settings)),
                            ("PDU 8", copy.deepcopy(pdu_settings)),
                            ("PDU 9", copy.deepcopy(pdu_settings)),
                            ("PDU 10", copy.deepcopy(pdu_settings)),
                            ("PDU 11", copy.deepcopy(pdu_settings)),
                            ("PDU 12", copy.deepcopy(pdu_settings)),
                            ("PDU 13", copy.deepcopy(pdu_settings)),
                            ("PDU 14", copy.deepcopy(pdu_settings)),
                            ("PDU 15", copy.deepcopy(pdu_settings)),
                            ("Real channels", collections.OrderedDict([
                                ("Quadrants", [11,16,21,26]),
                                ("Quadrant 1 tiles LV", [12,13,14,15]),
                                ("Quadrant 2 tiles LV", [17,18,19,20]),
                                ("Quadrant 3 tiles LV", [22,23,24,25]),
                                ("Quadrant 4 tiles LV", [27,28,29,30]),
                                ("Quadrant 1 tiles HV", [1,2,3,4]),
                                ("Quadrant 2 tiles HV", [5,6,7,8]),
                                ("Quadrant 3 tiles HV", [9,10,11,12]),
                                ("Quadrant 4 tiles HV", [13,14,15,16]),
                                ("PDUID", 10)
                                ])
                            ),
                            ("USB port", "/dev/ttyUSB0"),
                            ("Use USB", False),
                            ("IP address", "192.168.121.2"),
                            ("Ethernet port", 32000),
                            ("Recv buffer size", 96),
                            ("Socket timeout (secs)", 5.0)
                        ])

        for pdu in range(16):
            default_settings["PDU %d" % pdu]["Human name"] = "PDU %d" % pdu
        
        midas.frontend.EquipmentBase.__init__(self, midas_client, equip_name, default_common, default_settings)

        if use_fake_device:
            self.driver = FakeControlModuleDriver()
        else:
            self.driver = control_module_driver.ControlModule()

        if verbose:
            self.driver.verbose = True

        self.prev_settings = default_settings.copy()
        
        self.readback = collections.OrderedDict([
            ("PDU 0", copy.deepcopy(pdu_readback)),
            ("PDU 1", copy.deepcopy(pdu_readback)),
            ("PDU 2", copy.deepcopy(pdu_readback)),
            ("PDU 3", copy.deepcopy(pdu_readback)),
            ("PDU 4", copy.deepcopy(pdu_readback)),
            ("PDU 5", copy.deepcopy(pdu_readback)),
            ("PDU 6", copy.deepcopy(pdu_readback)),
            ("PDU 7", copy.deepcopy(pdu_readback)),
            ("PDU 8", copy.deepcopy(pdu_readback)),
            ("PDU 9", copy.deepcopy(pdu_readback)),
            ("PDU 10", copy.deepcopy(pdu_readback)),
            ("PDU 11", copy.deepcopy(pdu_readback)),
            ("PDU 12", copy.deepcopy(pdu_readback)),
            ("PDU 13", copy.deepcopy(pdu_readback)),
            ("PDU 14", copy.deepcopy(pdu_readback)),
            ("PDU 15", copy.deepcopy(pdu_readback))
        ])
                                                 
        self.prev_readback = copy.deepcopy(self.readback)
        self.odb_readback_dir = self.odb_settings_dir.replace("Settings", "Readback")
        self.client.odb_set(self.odb_readback_dir, self.readback)

        self.have_updates = [False] * 16

        # Tell the driver about our channel map
        self.validate_channel_map()

        # Push our ODB settings to the driver
        print("Forcing ODB settings to driver")
        self.force_write_to_driver = True
        self.settings_changed_func()
        self.force_write_to_driver = False

        # Force settings to be sent over SPI and readback into ODB
        print("Forcing SPI write and readback")
        self.readout_func()

    def validate_channel_map(self):
        """
        Validate that the user hasn't set the same channel multiple times.
        """
        q_idxs = self.settings["Real channels"]["Quadrants"]
        q1_idxs = self.settings["Real channels"]["Quadrant 1 tiles LV"]
        q2_idxs = self.settings["Real channels"]["Quadrant 2 tiles LV"]
        q3_idxs = self.settings["Real channels"]["Quadrant 3 tiles LV"]
        q4_idxs = self.settings["Real channels"]["Quadrant 4 tiles LV"]
        q1_hv_idxs = self.settings["Real channels"]["Quadrant 1 tiles HV"]
        q2_hv_idxs = self.settings["Real channels"]["Quadrant 2 tiles HV"]
        q3_hv_idxs = self.settings["Real channels"]["Quadrant 3 tiles HV"]
        q4_hv_idxs = self.settings["Real channels"]["Quadrant 4 tiles HV"]
        id_idx = self.settings["Real channels"]["PDUID"]
        
        all_lv_idxs = []
        all_lv_idxs.extend(q_idxs)
        all_lv_idxs.extend(q1_idxs)
        all_lv_idxs.extend(q2_idxs)
        all_lv_idxs.extend(q3_idxs)
        all_lv_idxs.extend(q4_idxs)
        all_lv_idxs.append(id_idx)

        all_hv_idxs = []
        all_hv_idxs.extend(q1_hv_idxs)
        all_hv_idxs.extend(q2_hv_idxs)
        all_hv_idxs.extend(q3_hv_idxs)
        all_hv_idxs.extend(q4_hv_idxs)

        all_lv_idxs = sorted(all_lv_idxs)
        all_hv_idxs = sorted(all_hv_idxs)

        self.driver.set_real_channels_quadrants(q_idxs)
        self.driver.set_real_channels_quadrant_tiles(1, q1_idxs, q1_hv_idxs)
        self.driver.set_real_channels_quadrant_tiles(2, q2_idxs, q2_hv_idxs)
        self.driver.set_real_channels_quadrant_tiles(3, q3_idxs, q3_hv_idxs)
        self.driver.set_real_channels_quadrant_tiles(4, q4_idxs, q4_hv_idxs)
        self.driver.set_real_channel_pduid(id_idx)

        for i in range(1, len(all_lv_idxs)):
            if all_lv_idxs[i] == all_lv_idxs[i-1]:
                raise ValueError("Invalid channel mapping - LV pin %s appears more than once" % all_lv_idxs[i])

        for i in range(1, len(all_hv_idxs)):
            if all_hv_idxs[i] == all_hv_idxs[i-1]:
                raise ValueError("Invalid channel mapping - HV pin %s appears more than once" % all_hv_idxs[i])
            
    def readout_func(self):
        """
        This is called automatically by the frontend framework periodically.
        
        We tell the driver about any channels that need turning on/off, and
        update the ODB with any readback changes that have happened.
        """
        start = datetime.datetime.now()
        any_updates = True in self.have_updates

        if any_updates and self.settings["Read only"]:
            self.client.msg("ControlModule is in read-only mode - will not change which channels are on/off")

        for pdu in range(16):
            if not self.have_updates[pdu]:
                continue

            self.have_updates[pdu] = False

            if self.settings["Read only"] or not self.settings["PDU %d" % pdu]["Enable"]:
                continue

            self.driver.write_updates(pdu)

            self.readback["PDU %d" % pdu]["Quadrants"] = self.driver.rdb_quadrants(pdu)
            self.readback["PDU %d" % pdu]["Quadrant 1 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(pdu, 1)
            self.readback["PDU %d" % pdu]["Quadrant 2 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(pdu, 2)
            self.readback["PDU %d" % pdu]["Quadrant 3 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(pdu, 3)
            self.readback["PDU %d" % pdu]["Quadrant 4 tiles LV"] = self.driver.rdb_quadrant_tiles_lv(pdu, 4)
            self.readback["PDU %d" % pdu]["Quadrant 1 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(pdu, 1)
            self.readback["PDU %d" % pdu]["Quadrant 2 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(pdu, 2)
            self.readback["PDU %d" % pdu]["Quadrant 3 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(pdu, 3)
            self.readback["PDU %d" % pdu]["Quadrant 4 tiles HV"] = self.driver.rdb_quadrant_tiles_hv(pdu, 4)
            self.readback["PDU %d" % pdu]["PDUID"] = self.driver.rdb_pduid(pdu)
            
            if self.prev_readback["PDU %d" % pdu] != self.readback["PDU %d" % pdu]:
                self.client.odb_set(self.odb_readback_dir, self.readback)
            
        self.prev_settings = copy.deepcopy(self.settings)
        self.prev_readback = copy.deepcopy(self.readback)

        end = datetime.datetime.now()

        if any_updates:
            print("Took %s to apply settings and readback into ODB" % (end-start))

        return None
    
    def settings_changed_func(self):
        """
        See note in class documentation about why we don't tell the driver
        immediately about any changes to which channels should be on/off.
        """
        start = datetime.datetime.now()

        if self.settings["Real channels"] != self.prev_settings["Real channels"]:
            self.client.msg("Restart the frontend to pick up the changed channel configuration", True)

        driver_comm = self.driver.get_comm_mode()

        if self.settings["Use USB"]:
            if driver_comm != "USB" or self.settings["USB port"] != self.prev_settings["USB port"]:
                self.driver.change_usb_port(self.settings["USB port"])
        else:
            if driver_comm != "Ethernet" or self.settings["IP address"] != self.prev_settings["IP address"] or self.settings["Ethernet port"] != self.prev_settings["Ethernet port"]:
                self.driver.change_ethernet_settings(self.settings["IP address"], self.settings["Ethernet port"], self.settings["Recv buffer size"], self.settings["Socket timeout (secs)"])
    
        for pdu in range(16):
            if not self.settings["PDU %d" % pdu]["Enable"]:
                continue

            if self.force_write_to_driver or self.prev_settings["PDU %d" % pdu]["Quadrants"] != self.settings["PDU %d" % pdu]["Quadrants"]:
                self.driver.set_quadrants(pdu, self.settings["PDU %d" % pdu]["Quadrants"])
                self.have_updates[pdu] = True

            if self.force_write_to_driver or self.prev_settings["PDU %d" % pdu]["PDUID"] != self.settings["PDU %d" % pdu]["PDUID"]:
                self.driver.set_pduid(pdu, self.settings["PDU %d" % pdu]["PDUID"])
                self.have_updates[pdu] = True

            for q in range(1,5):
                key_lv = "Quadrant %s tiles LV" % q
                key_hv = "Quadrant %s tiles HV" % q

                if self.force_write_to_driver or self.prev_settings["PDU %d" % pdu][key_lv] != self.settings["PDU %d" % pdu][key_lv]:
                    self.driver.set_quadrant_tiles_lv(pdu, q, self.settings["PDU %d" % pdu][key_lv])
                    self.have_updates[pdu] = True

                if self.force_write_to_driver or self.prev_settings["PDU %d" % pdu][key_hv] != self.settings["PDU %d" % pdu][key_hv]:
                    self.driver.set_quadrant_tiles_hv(pdu, q, self.settings["PDU %d" % pdu][key_hv])
                    self.have_updates[pdu] = True

        end = datetime.datetime.now()

        if True in self.have_updates:
            print("Took %s to write new settings to driver" % (end-start))
    
class ControlModuleFrontend(midas.frontend.FrontendBase):
    """
    Frontend class - we only have 1 equipment to control.
    """
    def __init__(self, use_fake_device, cm_name, verbose):
        prog_name = "ControlModule%s" % cm_name
        midas.frontend.FrontendBase.__init__(self, prog_name)
        self.add_equipment(ControlModuleEquipment(self.client, use_fake_device, prog_name, verbose))
    
if __name__ == "__main__":
    parser = midas.frontend.parser
    parser.add_argument("--use-fake-device", action="store_true")
    parser.add_argument("--cm-name", default="")
    parser.add_argument("--verbose", action="store_true")
    args = midas.frontend.parse_args()

    fe = ControlModuleFrontend(args.use_fake_device, args.cm_name, args.verbose)
    fe.run()