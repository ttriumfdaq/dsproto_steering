import serial
import socket
import time
import re
import binascii
import struct
import ctypes

class ControlModule():
    """
    Driver for the control module that can talk to 16 vPDUs (which have integrated
    steering modules).
    
    Members:
        * hv_on (char) - Value to send to turn a HV channel on.
        * hv_off (char) - Value to send to turn a HV channel off.
        * lv_on (char) - Value to send to turn a LV channel on.
        * lv_off (char) - Value to send to turn a LV channel off.

    Args:
    """
    def __init__(self):
        # For LV (HV5523) a "1" is off
        self.hv_on = '1'
        self.hv_off = '0'
        self.lv_on = '0'
        self.lv_off = '1'
        self.num_mux = 16
        
        self.real_channels = {"quadrants": [],
                              "q1 tiles lv": [],
                              "q2 tiles lv": [],
                              "q3 tiles lv": [],
                              "q4 tiles lv": [],
                              "q1 tiles hv": [],
                              "q2 tiles hv": [],
                              "q3 tiles hv": [],
                              "q4 tiles hv": [],
                              "pduid": None,
                            }

        self.verbose = False

        self.use_usb = None
        self.usb = None
        self.sock = None
        
        self.socket_timeout_secs = None
        self.arduino_ip = None
        self.arduino_port = None
        self.recv_buffer_size = None

        self.curr_mux = None

        self.pending_lv = {}
        self.pending_hv = {}

        for pdu in range(self.num_mux):
            self.pending_lv[pdu] = {i: False for i in range(1,33)}
            self.pending_hv[pdu] = {i: False for i in range(1,65)}

        self.rdb = {}

        for pdu in range(self.num_mux):
            self.rdb[pdu] = {
                "LV": {i: False for i in range(1,33)}, 
                "HV": {i: False for i in range(1,65)}
            }

    def get_comm_mode(self):
        """
        Get whether we're going to communicate with the Arduino
        via USB, Ethernet (or None if not configured yet).

        Use `change_usb_port()` or `change_ethernet_settings()`
        to change which method to use.
        """
        if self.use_usb is None:
            return None
        elif self.use_usb:
            return "USB"
        else:
            return "Ethernet"

    def change_usb_port(self, serial_port):
        """
        Change which USB port to talk to.

        Args:
            * serial_port (str) - e.g. "/dev/ttyUSB1"
        """
        self.use_usb = True

        if self.usb is not None:
            if self.usb.port == serial_port and self.usb.is_open:
                # Already opened this port
                return
            else:
                self.usb.close()

        print("Opening serial port %s" % serial_port)

        self.usb = serial.Serial(serial_port, 9600, timeout=1)
        close_to_end_of_preamble = False

        # Read preamble until we reach an empty line after "Initial settings:"
        while True:
            line = self.read_line()

            if line is None:
                time.sleep(0.1)
                continue

            if line.startswith("Initial settings:"):
                close_to_end_of_preamble = True
            
            if line == "" and close_to_end_of_preamble:
                break

        print("Finished reading preamble")

    def change_ethernet_settings(self, ip, port, recv_buffer_size, timeout_secs):
        self.use_usb = False
        self.arduino_ip = ip
        self.arduino_port = port
        self.recv_buffer_size = recv_buffer_size
        self.socket_timeout_secs = timeout_secs

    def set_real_channels_quadrants(self, one_based_index_list):
        """
        Set mapping between quadrants 1-4 and LV pins that control FE electronics.

        Args:
            * one_based_index_list (list of int) - 1 to 32. Length 4. Order is quadrant order.
        """
        self.real_channels["quadrants"] = [x for x in one_based_index_list]

    def set_real_channels_quadrant_tiles(self, quadrant, one_based_index_list_lv, one_based_index_list_hv):
        """
        Set mapping LV and HV pins for tiles in a quadrant.

        Args:
            * quadrant (int) - 1 to 4
            * one_based_index_list_lv (list of int) - 1 to 32. Length 4. Order is tile order.
            * one_based_index_list_hv (list of int) - 1 to 64. Length 4. Order is tile order.
        """
        self.real_channels["q%s tiles lv" % quadrant] = [x for x in one_based_index_list_lv]
        self.real_channels["q%s tiles hv" % quadrant] = [x for x in one_based_index_list_hv]

    def set_real_channel_pduid(self, one_based_index_lv):
        """
        Set which LV pin is used to control PDUID functionality.
        """
        self.real_channels["pduid"] = one_based_index_lv

    def quadrants_chan_list(self):
        """
        Get LV pins that control FE electronics for each quadrant.

        Returns:
            list of int - 1 to 32. Length 4. Order is quadrant order.
        """
        return self.real_channels["quadrants"]
        
    def quadrant_tiles_chan_list_lv(self, quadrant):
        """
        Get LV pins that control LV channels for a quadrant.

        Args:
            * quadrant (int) - 1 to 4

        Returns:
            list of int - 1 to 32. Length 4. Order is tile order.
        """
        return self.real_channels["q%s tiles lv" % quadrant]
        
    def quadrant_tiles_chan_list_hv(self, quadrant):
        """
        Get LV pins that control HV channels for a quadrant.

        Args:
            * quadrant (int) - 1 to 4

        Returns:
            list of int - 1 to 64. Length 4. Order is tile order.
        """
        return self.real_channels["q%s tiles hv" % quadrant]

    def pduid_chan(self):
        """
        Get LV pin that controls LV channel for PDUID.

        Returns:
            int - 1 to 32.
        """
        return self.real_channels["pduid"]

    def flush_usb(self):
        """
        Flush all output from serial buffer.
        """
        if self.verbose:
            print("Flushing output")

        self.usb.read_all()

    def read_line(self):
        """
        Read a line from the serial port. Trims the final newline.

        Returns:
            str
        """
        raw = self.usb.readline()

        if raw == b'':
            # Timeout (a true empty line would be b'\n')
            return None
        
        return str(raw, "utf-8").strip()

    def send_usb_command(self, command, num_readback_lines=2, readback_until_contains=None, write_timeout=0.1, read_timeout=0.1):
        """
        Send a command to the control module Arduino and read back the response.

        Args:
            * command (str) - Newline will be appended automatically
            * num_readback_lines (int) - How many lines to readback
            * readback_until_contains (str/None) - 
                If None, read back num_readback_lines lines of output.
                If str, read back until a line containing the specified text is found, ignoring the num_readback_lines parameter.
            * write_timeout (float)
            * read_timeout (float)
        
        Returns:
            list of str
        """
        
        # Most times the second readback line is empty.
        self.usb.write_timeout = write_timeout
        self.usb.timeout = read_timeout

        if self.verbose:
            print("Sending `%s`" % command)

        self.usb.write(bytes("%s\n" % command, "utf-8"))

        rdb = []

        if readback_until_contains is not None:
            # Read until a line starting with specified string
            while True:
                line = self.read_line()
                rdb.append(line)

                if line.find(readback_until_contains) != -1:
                    break      
        else:
            # Read set number of lines
            while len(rdb) < num_readback_lines:
                line = self.read_line()

                if line is None:
                    break
                else:            
                    rdb.append(line)
        
        if self.verbose:
            print("Read %s" % rdb)

        return rdb

    def write_updates_usb(self, pdu):
        self.flush_usb()

        all_lv_on = False not in self.pending_lv[pdu].values()
        all_hv_on = False not in self.pending_hv[pdu].values()
        all_lv_off = True not in self.pending_lv[pdu].values()
        all_hv_off = True not in self.pending_hv[pdu].values()

        if all_lv_on and all_hv_on:
            # Faster to turn on all channels with one command
            self.send_usb_command("on")
        elif all_lv_off and all_hv_off:
            # Faster to turn off all channels with one command
            self.send_usb_command("off")
        else:
            # Set each channel individually (but only send the
            # channels that differ from the current state)
            self.readback_expected_usb(pdu)

            for chan, bool_val in self.pending_lv[pdu].items():
                if bool_val != self.rdb[pdu]["LV"][chan]:
                    str_val = self.lv_on if bool_val else self.lv_off
                    self.send_usb_command("lv %d %s" % (chan, str_val))

            for chan, bool_val in self.pending_hv[pdu].items():
                if bool_val != self.rdb[pdu]["HV"][chan]:
                    str_val = self.hv_on if bool_val else self.hv_off
                    self.send_usb_command("hv %d %s" % (chan, str_val))

        # Read the full state we expect to have sent
        self.readback_expected_usb(pdu)

        # Last line is either 
        # SPI Write Successful!
        # or
        # ERROR: SPI Write Unsuccessful
        resp = self.send_usb_command("write", readback_until_contains="SPI")
        success = False

        if resp[-1].find("SPI Write Successful!") != -1:
            success = True
        else:
            print("Failed to apply changes over SPI! Response was: %s" % resp)

        if not success:
            # Look for lines like 
            # For subarray 11, we wanted to see 0, but instead we saw 255
            # that tell us the difference between what we sent and received
            for line in resp:
                match = re.search("For subarray (\d+), we wanted to see (\d+), but instead we saw (\d+)", line)
                
                if match:
                    subarray = int(match.group(1))
                    saw = int(match.group(3))

                    if subarray < 4:
                        lv_hv = "LV"
                        chan_offset = subarray * 8
                        on_val = self.lv_on
                    else:
                        lv_hv = "HV"
                        chan_offset = (subarray - 4) * 8
                        on_val = self.hv_on
                    
                    for i in range(8):
                        bit = (saw >> i) & 0x1
                        is_on = (str(bit) == on_val)

                        # Update the readback state with the value we read.
                        # rdb uses 1-based indices to match what Arduino uses.
                        self.rdb[pdu][lv_hv][chan_offset + i + 1] = is_on

        if self.verbose:
            print("Readback for PDU %d is %s" % (pdu, self.rdb[pdu]))

        return success

    def write_updates(self, pdu):
        """
        Write the new LV/HV settings that have been previously set with
        set_quadrants(), set_lv() etc.

        Updates the readback state in `self.rdb`.
        """
        print("Writing updates for PDU %d" % pdu)
        self.switch_mux(pdu)

        if self.use_usb:
            self.write_updates_usb(pdu)
        else:
            self.write_updates_ethernet(pdu)

    def readback_expected_usb(self, pdu):
        """
        Read the state that the Arduino will send over SPI.

        Populates `self.rdb`
        """
        resp = self.send_usb_command("print", readback_until_contains="High Voltage SiPM")

        # Arrays are printed left-to-right in increasing channel number.
        lv_prefix = "Low Voltage Amplifier Bias Array (binary) is: "
        hv_prefix = "High Voltage SiPM Bias Array (binary) is: "

        for line in resp:
            if line.startswith(lv_prefix):
                lv_bits = [x for x in line.replace(lv_prefix, "")]
                for idx, lv_bit in enumerate(lv_bits):
                    self.rdb[pdu]["LV"][idx+1] = (lv_bit == self.lv_on)
            elif line.startswith(hv_prefix):
                hv_bits = [x for x in line.replace(hv_prefix, "")]
                for idx, hv_bit in enumerate(hv_bits):
                    self.rdb[pdu]["HV"][idx+1] = (hv_bit == self.hv_on)

    def switch_mux(self, pdu):
        """
        Change which PDU will be controlled when calling `write_updates()`.

        Args:
            * pdu (int) - 0 to 15
        """
        if pdu == self.curr_mux:
            return

        self.curr_mux = pdu

        if self.use_usb:
            self.flush_usb()
            self.send_usb_command("mux %d" % pdu)
        else:
            msg = self.pack_ethernet_msg("M", text=f' {pdu : <23}')
            self.send_ethernet(msg)
 
    def set_quadrants(self, pdu, bool_list):
        """
        Set the LV state of the FE electronics for each quadrant.
        
        * Call `write_updates()` afterwards to push the new settings
          out over SPI!
        
        Args:
            * pdu (int) - 0 to 15
            * bool_list (list of bool) - Length 4. Whether each quadrant should be on/off.
        """
        print("Updating FE electronics LV state for PDU %d" % pdu)
        self.set_lv(pdu, self.quadrants_chan_list(), bool_list)

    def set_quadrant_tiles_lv(self, pdu, quadrant, bool_list):
        """
        Set the LV state of each tile in a quadrant.
        
        * Call `write_updates()` afterwards to push the new settings
          out over SPI!
        
        Args:
            * quadrant (int)
            * bool_list (list of bool) - Length 4. Whether LV for each tile should be on/off.
        """
        print("Updating quadrant %d LV state for PDU %d" % (quadrant, pdu))
        self.set_lv(pdu, self.quadrant_tiles_chan_list_lv(quadrant), bool_list)

    def set_quadrant_tiles_hv(self, pdu, quadrant, bool_list):
        """
        Set the HV state of each tile in a quadrant.
        
        * Call `write_updates()` afterwards to push the new settings
          out over SPI!
        
        Args:
            * pdu (int) - 0 to 15
            * quadrant (int)
            * bool_list (list of bool) - Length 4. Whether HV for each tile should be on/off.
        """
        print("Updating quadrant %d HV state for PDU %d" % (quadrant, pdu))
        self.set_hv(pdu, self.quadrant_tiles_chan_list_hv(quadrant), bool_list)

    def set_pduid(self, pdu, state):
        """
        Set the LV state of the FE electronics for the PDUID pin.
        
        * Call `write_updates()` afterwards to push the new settings
          out over SPI!
        
        Args:
            * pdu (int) - 0 to 15
            * state (bool) - Whether the pin should be on/off.
        """
        print("Updating PDUID LV state for PDU %d" % pdu)
        self.set_lv(pdu, [self.pduid_chan()], [state])

    def rdb_quadrants(self, pdu):
        """
        Get the actual state of the LV pins for frontend electronics for each
        quadrant.

        Readback state is updated when you call `write_updates()`.

        Args:
            * pdu (int)

        Returns:
            * list of bool - Length 4. Whether each quadrant is on/off.
        """
        return [self.rdb[pdu]["LV"][x] for x in self.quadrants_chan_list()]

    def rdb_quadrant_tiles_lv(self, pdu, quadrant):
        """
        Get the actual state of the LV pins for the tiles in a quadrant

        Readback state is updated when you call `write_updates()`.

        Args:
            * pdu (int)
            * quadrant (int)

        Returns:
            * list of bool - Length 4. Whether each tile is on/off.
        """
        return [self.rdb[pdu]["LV"][x] for x in self.quadrant_tiles_chan_list_lv(quadrant)]

    def rdb_quadrant_tiles_hv(self, pdu, quadrant):
        """
        Get the actual state of the HV pins for the tiles in a quadrant

        Readback state is updated when you call `write_updates()`.

        Args:
            * pdu (int)
            * quadrant (int)
            
        Returns:
            * list of bool - Length 4. Whether each tile is on/off.
        """
        return [self.rdb[pdu]["HV"][x] for x in self.quadrant_tiles_chan_list_hv(quadrant)]

    def rdb_pduid(self, pdu):
        """
        Get the actual state of the LV pins for frontend electronics for reading the PDUID.

        Readback state is updated when you call `write_updates()`.

        Args:
            * pdu (int)

        Returns:
            * bool. Whether the pin is on/off.
        """
        return self.rdb[pdu]["LV"][self.pduid_chan()]
    
    def set_lv(self, pdu, chan_list, bool_list):
        """
        Update our internal cache of LV settings to send to the Arduino.

        Settings are only actually send when you call `write_updates()`.

        Args:
            * pdu (int) - 0 to 15
            * chan_list (list of int) - 1-32
            * bool_list (list of int)
        """
        for idx, chan in enumerate(chan_list):
            self.pending_lv[pdu][chan] = bool_list[idx]

    def set_hv(self, pdu, chan_list, bool_list):
        """
        Update our internal cache of LV settings to send to the Arduino.

        Settings are only actually send when you call `write_updates()`.

        Args:
            * pdu (int) - 0 to 15
            * chan_list (list of int) - 1-64
            * bool_list (list of int)
        """
        for idx, chan in enumerate(chan_list):
            self.pending_hv[pdu][chan] = bool_list[idx]
        
    def pack_ethernet_msg(self, msg_type, values=None, text=None):
        if text is None:
            if values is not None:
                text = values
            else:
                text = self.bin_to_hexstr(['0']*96)
        
        bytes24 = bytes(text, "utf-8")
        bytes24 += b'\0' * (24 - len(bytes24))
        
        msg = bytes(msg_type, "utf-8") + bytes24
        crc = binascii.crc32(msg)
        msg = struct.pack("<c24sI", bytes(msg_type, "utf-8"), bytes24, crc)
        return msg

    def unpack_ethernet_resp(self, resp):
        status, values, crc = struct.unpack("<c24sI", resp[0])        
        
        if crc != binascii.crc32(resp[0][0:25]):
            raise RuntimeError("ERROR: CRC mismatch in response from Arduino!")

        # S = Success, E = Error
        if status.decode('utf-8') != 'S':
            raise RuntimeError("ERROR: Got status '%s' not 'S'" % status.decode('utf-8'))

        return values

    def send_ethernet(self, msg):
        if self.verbose:
            print("Sending %s" % msg)

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(self.socket_timeout_secs)
        sock.sendto(msg, (self.arduino_ip, self.arduino_port))
        resp = sock.recvfrom(self.recv_buffer_size)
        values = self.unpack_ethernet_resp(resp)
        return values

    def write_updates_ethernet(self, pdu):
        """
        Main function for communicating with the TRIUMF arduino and turning channels
        on or off.
        
        Calls handle_readback() with the result of reading which channels are
        currently on, which updates self.rdb_channel_on.

        Args:
            * pdu (int) - 0 to 15
        """
        write_array = ['0'] * 96
        for lv_chan, state in self.pending_lv[pdu].items():
            write_array[lv_chan - 1] = self.lv_on if state else self.lv_off

        for hv_chan, state in self.pending_hv[pdu].items():
            write_array[hv_chan - 1 + 32] = self.hv_on if state else self.hv_off

        # 96 bits to 24 chars
        values = self.bin_to_hexstr(write_array)
        msg = self.pack_ethernet_msg("W", values)
        values = self.send_ethernet(msg)

        # 24 chars to 96 bits
        rdb_str = values.decode('utf8')

        if self.verbose:
            print("Readback %s" % rdb_str)

        rdb_array = self.hexstr_to_bin(rdb_str)

        return self.handle_ethernet_readback(pdu, rdb_array)

    def handle_ethernet_readback(self, pdu, rdb_array):
        """
        Take a raw array returned by the arduino, and convert to whether each
        channel is on or off. Populates self.rdb_channel_on.
        
        Args:
            * rdb_array (list of char) - Expect 96 values.
        """
        lv_states = rdb_array[:32]
        hv_states = rdb_array[32:]

        for idx, state in enumerate(lv_states):
            self.rdb[pdu]["LV"][idx+1] = (state == self.lv_on)

        for idx, state in enumerate(hv_states):
            self.rdb[pdu]["HV"][idx+1] = (state == self.hv_on)
        
    def hexstr_to_bin(self, hexstr):
        """
        Convert a 24-character hex string to a 96-element list of '1'/'0'.
        1/0 list is ordering 0->95.
        hexstr has groups of 4 starting 0,4,8... then bits within

        Returns:
            list of int
        """
        retval = []

        for i in range(24):
            char = hexstr[i]

            if char < '0' or char > 'F':
                # Old Arduino code incorrectly converted to hex
                # (by forgetting to >>4 the upper nibble of a byte
                # before converting to a character code). E.g. the
                # upper nibble of 0xFF became "'" rather than "F"...
                cval = ctypes.c_uint8(bytes(char, "utf8")[0])
                cval.value = cval.value + 10 - 0x41
                cval.value = cval.value >> 4
                int_dig = int(cval.value)
            else:
                int_dig = int(char, 16)

            for n in range(4):
                retval.append('1' if int_dig & (1<<n) else '0')

        return retval

    def bin_to_hexstr(self, bin_list):
        """
        Convert a 96-element list of '1'/'0' to a 24-character hex string.
        1/0 list has channel 0 first. hex string is little-endian (channel 0 last).

        Returns:
            string
        """
        retval = ""
        bin_list = [int(x) for x in bin_list]

        for i in range(24):
            val = (bin_list[(i*4)+0] << 0) | (bin_list[(i*4)+1] << 1) | (bin_list[(i*4)+2] << 2) | (bin_list[(i*4)+3] << 3)
            retval += chr(ord('0') + val if val < 10 else ord('A') + val - 10)

        return retval
