"""
A driver for talking to the Darkside Proto-0 steering module
(which directs one input voltage to multiple output channels).

It is based on a GUI originally written by "Eraguzin". The original 
documentation for that GUI is written below. This version is 
designed to have fewer magic numbers, and to allow control by a midas frontend
(see steering_module_frontend.py).


Original documentation:

This is a GUI designed to communicate with board IO-1733-1 Rev A, with a HV5523 -> HV3418 SPI control chain
Through an LTC6820 pair of chips to minimize the lines on the cable.  This script uses an FT2323H board:
https://www.adafruit.com/product/2264
To generate the SPI signals for the warm side LTC6820
This requirees the FT232H "libusb" driver to be installed, as well as the Python module "libftdi" as described here:
https://learn.adafruit.com/adafruit-ft232h-breakout/mpsse-setup
"""
import struct
import socket
import binascii

class NetworkSettingsBNL():
    """
    The BNL version of the arduino code has a very odd communication protocol defined.
    For each command we send, we create a new socket. The response we receive
    comes on a different socket (again, created newly for each command).
    
    Both the IP of the arduino AND the IP that the arduino responds to are
    fixed. It is unclear why we don't just use a bi-directional socket that we
    keep open. I'll repeat - the destination of the response from the arduino
    is fixed!
    
    This class is a helper struct for defining the appropriate settings.
    
    Members:
        * our_ip (str) - The IP that the arduino will send responses to.
        * recv_port (int) - The port that the arduino will send responses to.
        * socket_timeout_secs (float) - Timeout for socket connections.
        * arduino_ip (str) - The IP address of the arduino.
        * arduino_port (str) - The port the arduino will be listening on.
        * recv_buffer_size (int) - Max number of bytes we should receive from the arduino.
            Unclear why this defaults to 104 when we only have 96 channels.
    """
    def __init__(self):
        self.our_ip = None
        self.recv_port = None
        self.socket_timeout_secs = None
        self.arduino_ip = None
        self.arduino_port = None
        self.recv_buffer_size = None 

class NetworkSettingsTRIUMF():
    """
    The TRIUMF version of the arduino code uses a much simpler communication protocol,
    with only a single socket connection for both request and reply.

    Members:
        * socket_timeout_secs (float) - Timeout for socket connections.
        * arduino_ip (str) - The IP address of the arduino.
        * arduino_port (str) - The port the arduino will be listening on.
        * recv_buffer_size (int) - Max number of bytes we should receive from the arduino.
    """
    def __init__(self):
        self.socket_timeout_secs = None
        self.arduino_ip = None
        self.arduino_port = None
        self.recv_buffer_size = None

class SteeringModule():
    """
    Driver for the steering module.
    
    Channel numbers are 0-31 for LV, 32+ for HV.
    In the ODB (what was previously a config file), we do not specify the 32
    offset, and start indexing from 1. So HV channel 1 is overall 32. LV
    channel 1 is overall 0.
    
    Members:
        * hv_on (char) - Value to send to turn a HV channel on.
        * hv_off (char) - Value to send to turn a HV channel off.
        * lv_on (char) - Value to send to turn a LV channel on.
        * lv_off (char) - Value to send to turn a LV channel off.
        * hv_offset (int) - Offset to add to HV channel numbers.
        * max_chan (int) - Maximum number of channels supported (and the number
            of values we'll send to the arduino).
        * real_channels (dict) - Which array index in the values we send to the
            arduino (0-95) match to the "channels 0-24" that the user sees.
        * rdb_channel_on (dict of {int: bool}) - Which of the 96 channels are
            currently on, according to the response we got from the arduino.
        * write_array (list of char) - The values we'll send to the arduino.
        * bnl_mode (bool) - Whether to use BNL-style or TRIUMF-style communication 
            protocol.
        * network (`NetworkSettingsBNL` or `NetworkSettingsTRIUMF`) - How to talk 
            to the arduino.
    """
    def __init__(self, bnl_mode, network_settings, use_pduplus_map):
        # For LV (HV5523) a "1" is off
        self.hv_on = '1'
        self.hv_off = '0'
        self.lv_on = '0'
        self.lv_off = '1'
        self.hv_offset = 32
        self.max_chan = 96

        self.use_pduplus_map = use_pduplus_map

        if self.use_pduplus_map:
            self.real_channels = {"quadrants": [],
                                  "q1 tiles lv": [],
                                  "q2 tiles lv": [],
                                  "q3 tiles lv": [],
                                  "q4 tiles lv": [],
                                  "q1 tiles hv": [],
                                  "q2 tiles hv": [],
                                  "q3 tiles hv": [],
                                  "q4 tiles hv": []}
        else:
            self.real_channels = {"uc": None,
                                  "amp": None,
                                  "lv": [],
                                  "hv": []}
        
        self.rdb_channel_on = {i: False for i in range(self.max_chan)}
        self.write_array = [self.off_value_for_chan(chn) for chn in range(self.max_chan)]
        self.bnl_mode = bnl_mode
        self.network = network_settings

    def set_real_channel_uc(self, one_based_index):
        """
        Define which real LV channel (1-32) controls the uC regulator.
        
        Args:
            * one_based_index (int) - 1-32
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDU+ function")
        self.real_channels["uc"] = one_based_index - 1
        
    def set_real_channel_amp(self, one_based_index):
        """
        Define which real LV channel (1-32) controls the amp regulator.
        
        Args:
            * one_based_index (int) - 1-32
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDU+ function")
        self.real_channels["amp"] = one_based_index - 1
        
    def set_real_channels_lv(self, one_based_index_list):
        """
        Define which real LV channels (1-32) correspond to each channel users
        will refer to as channel 0 etc.
        
        Args:
            * one_based_index_list (list of int) - 1-32 * 25
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDU+ function")
        self.real_channels["lv"] = [x - 1 for x in one_based_index_list]
        
    def set_real_channels_quadrants(self, one_based_index_list):
        if not self.use_pduplus_map:
            raise RuntimeError("This is a PDU+ function")
        self.real_channels["quadrants"] = [x - 1 for x in one_based_index_list]

    def set_real_channels_quadrant_tiles(self, quadrant, one_based_index_list_lv, one_based_index_list_hv):
        if not self.use_pduplus_map:
            raise RuntimeError("This is a PDU+ function")
        self.real_channels["q%s tiles lv" % quadrant] = [x - 1 for x in one_based_index_list_lv]
        self.real_channels["q%s tiles hv" % quadrant] = [x + self.hv_offset - 1 for x in one_based_index_list_hv]

    def set_real_channels_hv(self, one_based_index_list):
        """
        Define which real HV channels (33-96) correspond to each channel users
        will refer to as channel 0 etc.
        
        Args:
            * one_based_index_list (list of int) - 33-96 * 25
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDU+ function")
        self.real_channels["hv"] = [x - 1 for x in one_based_index_list]

    def all_chan_list(self):
        """
        Get the list of all "real channels" that are mapped to user-facing
        channels (including uC, amp, LV and HV).
        
        Returns:
            list of int (0-95)
        """
        retval = self.orig_lv_chan_list(False)
        retval.extend(self.orig_hv_chan_list())
        return retval
    

    def quadrants_chan_list(self):
        return self.real_channels["quadrants"]
        
    def quadrant_tiles_chan_list_lv(self, quadrant):
        return self.real_channels["q%s tiles lv" % quadrant]
        
    def quadrant_tiles_chan_list_hv(self, quadrant):
        return self.real_channels["q%s tiles hv" % quadrant]

    def orig_lv_chan_list(self, exclude_uc_and_amp=True):
        """
        Get the list of all "real channels" that are mapped to user-facing
        LV channels.
        
        Args:
            * exclude_uc_and_amp (bool) - Whether to include the special
                uc regulator and amp regulator values in the list.
        
        Returns:
            list of int (0-95; expect 0-31 only)
        """
        retval = []

        if not exclude_uc_and_amp:
            retval.extend([self.real_channels["uc"], self.real_channels["amp"]])
        retval.extend(self.real_channels["lv"])

        return retval
    
    def orig_hv_chan_list(self):
        """
        Get the list of all "real channels" that are mapped to user-facing
        HV channels.
        
        Returns:
            list of int (0-95; expect 32-95 only)
        """
        return self.real_channels["hv"]

    def is_chan_hv(self, chn):
        """
        Whether the given channel is for HV or not.
        
        Args:
            * chn (int) - 0-95; 0-31 are LV, 32-95 are HV.
            
        Returns:
            bool
        """
        return chn >= self.hv_offset
    
    def off_value_for_chan(self, chn):
        """
        Get the character that represents 'off' for this channel.
        
        Args:
            * chn (int) - 0-95; 0-31 are LV, 32-95 are HV.
            
        Returns:
            char
        """
        return self.hv_off if self.is_chan_hv(chn) else self.lv_off
    
    def on_value_for_chan(self, chn):
        """
        Get the character that represents 'on' for this channel.
        
        Args:
            * chn (int) - 0-95; 0-31 are LV, 32-95 are HV.
            
        Returns:
            char
        """
        return self.hv_on if self.is_chan_hv(chn) else self.lv_on
           
    def write_Arduino_BNL(self, function):
        """
        Main function for communicating with the BNL arduino and turning channels
        on or off.
        
        Calls handle_readback() with the result of reading which channels are
        currently on, which updates self.rdb_channel_on.
        
        Args:
            * function (str) - "set" or "write" - unclear what the distinction
                is, and when we should actually expect the update to take
                place.
        """
        #Set up listening socket before anything else - IPv4, UDP
        sock_readresp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #Allows us to quickly access the same socket and ignore the usual OS wait time betweeen accesses
        sock_readresp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
        
        #Prepare to listen at configuration socket and for the correct port for the board we're writing to
        sock_readresp.bind((self.network.our_ip, self.network.recv_port))
        sock_readresp.settimeout(self.network.socket_timeout_secs)

        send_array = [j.encode("utf-8") for j in self.write_array] 
        
        if (function == "set"):
            MESSAGE = struct.pack("c{}c".format(len(send_array)), b's', *send_array)
        elif (function == "write"):
            MESSAGE = struct.pack("c",b'w')
            
        print("Message is {}".format(MESSAGE))
        
        sock_read = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock_read.setblocking(0)
        sock_read.sendto(MESSAGE, (self.network.arduino_ip, self.network.arduino_port))
        sock_read.close()
        
        try:
            data = sock_readresp.recv(self.network.recv_buffer_size)
        except socket.timeout:
            print ("Error: No read packet received from board, quitting")
            print ("Waited for response on {}".format(sock_readresp.getsockname()))
            sock_readresp.close()
            return None
        
        # TODO - why different set/write? Expect to fail before sending "write"?
        readback_array = list(data.decode("utf-8"))
        print("Readback is {}".format(readback_array))
                
        if self.write_array != readback_array:
            print("Error: Return array is {}".format(readback_array))
            
        self.handle_readback(readback_array)
        
    def pack_arduino_TRIUMF_msg(self, msg_type, values):
        msg = bytes(msg_type, "utf-8") + bytes(values, "utf-8")
        crc = binascii.crc32(msg)
        msg = struct.pack("<c24sI", bytes(msg_type, "utf-8"), bytes(values, "utf-8"), crc)
        return msg

    def unpack_arduino_TRIUMF_resp(self, resp):
        status, values, crc = struct.unpack("<c24sI", resp[0])        
        
        if crc != binascii.crc32(resp[0][0:25]):
            raise RuntimeError("ERROR: CRC mismatch in response from Arduino!")

        # S = Success, E = Error
        if status.decode('utf-8') != 'S':
            raise RuntimeError("ERROR: Got status '%s' not 'S'" % status.decode('utf-8'))

        return values

    def write_Arduino_TRIUMF(self):
        """
        Main function for communicating with the TRIUMF arduino and turning channels
        on or off.
        
        Calls handle_readback() with the result of reading which channels are
        currently on, which updates self.rdb_channel_on.
        """
        # 96 bits to 24 chars
        values = self.bin_to_hexstr(self.write_array)

        print("Sending  %s" % values)

        msg = self.pack_arduino_TRIUMF_msg("W", values)

        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(5)
        sock.sendto(msg, (self.network.arduino_ip, self.network.arduino_port))
        
        resp = sock.recvfrom(self.network.recv_buffer_size)
        values = self.unpack_arduino_TRIUMF_resp(resp)

        # 24 chars to 96 bits
        rdb_str = values.decode('utf8')
        print("Readback %s" % rdb_str)
        rdb_array = self.hexstr_to_bin(rdb_str)

        return self.handle_readback(rdb_array)

    def handle_readback(self, rdb_array):
        """
        Take a raw array returned by the arduino, and convert to whether each
        channel is on or off. Populates self.rdb_channel_on.
        
        Args:
            * rdb_array (list of char) - Expect 96 values.
        """
        for index, state in enumerate(rdb_array):
            if state == self.on_value_for_chan(index):
                self.rdb_channel_on[index] = True
            elif state == self.off_value_for_chan(index):
                self.rdb_channel_on[index] = False
            else:
                raise RuntimeError("ERROR: Something wrong with the readback truth array")

    def hexstr_to_bin(self, hexstr):
        """
        Convert a 24-character hex string to a 96-element list of '1'/'0'.
        1/0 list is ordering 0->95.
        hexstr has groups of 4 starting 0,4,8... then bits within

        Returns:
            list of int
        """
        retval = []

        for i in range(24):
            int_dig = int(hexstr[i], 16)
            
            for n in range(4):
                retval.append('1' if int_dig & (1<<n) else '0')

        return retval

    def bin_to_hexstr(self, bin_list):
        """
        Convert a 96-element list of '1'/'0' to a 24-character hex string.
        1/0 list has channel 0 first. hex string is little-endian (channel 0 last).

        Returns:
            string
        """
        retval = ""
        bin_list = [int(x) for x in bin_list]

        for i in range(24):
            val = (bin_list[(i*4)+0] << 0) | (bin_list[(i*4)+1] << 1) | (bin_list[(i*4)+2] << 2) | (bin_list[(i*4)+3] << 3)
            retval += chr(ord('0') + val if val < 10 else ord('A') + val - 10)

        return retval

    def rdb_orig_uc(self):
        """
        Whether the uC regulator is currently on, according to the most recent
        readback.
        
        Returns:
            bool
        """
        return self.rdb_channel_on[self.real_channels["uc"]]
    
    def rdb_orig_lv(self):
        """
        Whether each user-facing LV channel is on, according to the most recent
        readback.
        
        Returns:
            list of bool - expect 25 values, one for each user-facing channel
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDUPlus function")
        return [self.rdb_channel_on[x] for x in self.orig_lv_chan_list()]

    def rdb_orig_hv(self):
        """
        Whether each user-facing HV channel is on, according to the most recent
        readback.
        
        Returns:
            list of bool - expect 25 values, one for each user-facing channel
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDUPlus function")
        return [self.rdb_channel_on[x] for x in self.orig_hv_chan_list()]

    def rdb_quadrants(self):
        return [self.rdb_channel_on[x] for x in self.quadrants_chan_list()]

    def rdb_quadrant_tiles_lv(self, quadrant):
        return [self.rdb_channel_on[x] for x in self.quadrant_tiles_chan_list_lv(quadrant)]

    def rdb_quadrant_tiles_hv(self, quadrant):
        return [self.rdb_channel_on[x] for x in self.quadrant_tiles_chan_list_hv(quadrant)]

    def set_orig_lv(self, value_list, immediate_commit=True):
        """
        Set the LV channels to the specific state.
        
        Args:
            * value_list (list of bool) - expect 25 values.
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        self._update_multiple_channels(self.orig_lv_chan_list(), value_list, immediate_commit)
        
    def set_quadrants(self, value_list, immediate_commit=True):
        self._update_multiple_channels(self.quadrants_chan_list(), value_list, immediate_commit)

    def set_quadrant_tiles_lv(self, quadrant, value_list, immediate_commit=True):
        self._update_multiple_channels(self.quadrant_tiles_chan_list_lv(quadrant), value_list, immediate_commit)

    def set_quadrant_tiles_hv(self, quadrant, value_list, immediate_commit=True):
        self._update_multiple_channels(self.quadrant_tiles_chan_list_hv(quadrant), value_list, immediate_commit)

    def set_orig_hv(self, value_list, immediate_commit=True):
        """
        Set the HV channels to the specific state.
        
        Args:
            * value_list (list of bool) - expect 25 values.
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        self._update_multiple_channels(self.orig_hv_chan_list(), value_list, immediate_commit)
        
    def set_uc(self, value, immediate_commit=True):
        """
        Set the uC regulator to the specific state.
        
        Args:
            * value (bool)
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        if self.use_pduplus_map:
            raise RuntimeError("Not a PDU+ function")
        self._update_channel(self.real_channels["uc"], value, immediate_commit)
    
    def write_updates(self):
        """
        Actually commit any change to self.write_array to the arduino
        """
        if self.bnl_mode:
            self.write_Arduino_BNL("set")
            self.write_Arduino_BNL("write")
        else:
            self.write_Arduino_TRIUMF()
        
    def _chn_off(self, chn, immediate_commit=True):
        """
        Turn a specific channel off.
        
        Args:
            * chn (int) - 0-95
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        self._update_channel(chn, self.off_value_for_chan(chn), immediate_commit)
        
    def _chn_on(self, chn, immediate_commit=True):
        """
        Turn a specific channel on.
        
        Args:
            * chn (int) - 0-95
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        self._update_channel(chn, self.on_value_for_chan(chn), immediate_commit)
        
    def _writable_value(self, chn, value):
        """
        Convert a bool to a character we want to send to the arduino.
        
        Args:
            * chn (int) - 0-95
            * value (bool or char)
            
        Returns:
            char
        """
        if value is True:
            return self.on_value_for_chan(chn)
        elif value is False:
            return self.off_value_for_chan(chn)
        else:
            return value

    def _update_channel(self, chn, value, immediate_commit=True):
        """
        Actually update a channel to be on/off.
        
        Args:
            * chn (int) - 0-95
            * value (bool or char)
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        self.write_array[chn] = self._writable_value(chn, value)
        
        if immediate_commit:
            self.write_updates()
            
    def _update_multiple_channels(self, chn_list, value_list, immediate_commit=True):
        """
        Actually update a set channels to be on/off.
        
        Edge case - probably won't do the write thing is you pass in a single
        value for value_list, and chn_list contains a mixture of LV and HV
        channels!
        
        Args:
            * chn_list (int) - 0-95
            * value_list (bool/char/list of bool/list of char) - If a list, it
                should be the same length as chn_list, and we'll match up the
                new state to the channel at the same index in chn_list. If a
                single value, we'll apply that value to all channels.
            * immediate_commit (bool) - Whether to write to arduino immediately.
        """
        if not isinstance(value_list, list):
            value_list = [value_list] * len(chn_list)
            
        if len(chn_list) != len(value_list):
            raise ValueError("Invalid value list - %s channels provided, but %s values" % (len(chn_list), len(value_list)))
        
        for i, chn in enumerate(chn_list):
            self.write_array[chn] = self._writable_value(chn, value_list[i])
                 
        if immediate_commit:
            self.write_updates()   