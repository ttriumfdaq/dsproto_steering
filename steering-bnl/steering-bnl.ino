//Eric Raguzin
//Brookhaven National Laboratory
//Control for the IO-1733-1 PCB serial chain for Darkside test with LTC6820 interface and FeatherWing Ethernet shield
//8/14/19
// Lightly edited by Gennaro Tortone to specify network settings appropriate to INFN Napoli.
#include <Ethernet.h>
#include <EthernetUdp.h>
//Datasheets available here:
//http://ww1.microchip.com/downloads/en/DeviceDoc/hv5523.pdf
//http://ww1.microchip.com/downloads/en/DeviceDoc/20005843A.pdf
//https://www.analog.com/media/en/technical-documentation/data-sheets/6820fb.pdf

//LTC6820 must be in the POL=0, PHA=1 positions.  Full explanation at bottom of file

//Adjust speed of SPI write (to compensate for any issues in cryo)/////////
int SPI_delay = 1;
bool to_print = true;
char high = '1';
char low = '0';
////////////////////////////////////////////////////////////////////////////
//To translate to LTC6820 language, MOSI is D_IN and DATA, MISO is D_OUT and READBACK

#define D_IN  4  //Data out
#define D_OUT 3  //Readback coming in
#define LE    9
#define CLK   2
#define UDP_PACKET_SIZE 96
#define ETHERNET_CS_PIN 10
#define BITS 96

//Todo low voltage and high voltage is not correct

//I use a char array for the 64+32 bit stream instead of a 64 bit int or something because of this : https://forum.arduino.cc/index.php?topic=496719.0
//It's 12 arrays of 8 bits each, so it's a little more complicated to access, but is essentially the same.
//It's organized so that the first 4 arrays are for the low voltage amplifier bias, the last 8 are for the high voltage SiPM bias
//When printed, the arrays go from Channel 1 on the left to Channel 32 or 64 on the right
//This defaults to all outputs NOT allowing high voltage (the two chips require opposite polarities for that).
//All off
byte array_to_set[12] = {255, 255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0};
//All on
//byte array_to_set[12] = {0,0,0,0,255,255,255,255,255,255,255,255};
byte array_readback[12] = {0};

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xEB
};
IPAddress ip(172, 16, 6, 155);
IPAddress dns(192, 84, 134, 55);
IPAddress gateway(172, 16, 1, 254);
IPAddress subnet(255, 255, 0, 0);

IPAddress Remoteip(172, 16, 13, 140);

unsigned int receivePort = 32000;      // local port to listen on
unsigned int sendPort = 32001;      // local port to listen on

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

void setup() {
  // You can use Ethernet.init(pin) to configure the CS pin
  Ethernet.init(ETHERNET_CS_PIN);  // Most Arduino shields

  // start the Ethernet
  Ethernet.begin(mac, ip, dns, gateway, subnet);

  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }

  // start UDP
  Udp.begin(receivePort);

  pinMode(D_IN, OUTPUT);
  pinMode(D_OUT, INPUT);
  pinMode(LE, OUTPUT);
  pinMode(CLK, OUTPUT);

  //Latch off
  //POL 0, PHA 0 requires CLK to idle low
  digitalWrite(LE, HIGH);
  digitalWrite(CLK, LOW);
  digitalWrite(D_IN, LOW);

  //Writes the initial array to the chips so that all outputs are off.  It has a habit of starting up in a state that allows HV through.
  write_chip(false);
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();

  if (packetSize) {
    Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    char *IncomingBuffer=malloc(UDP_PACKET_SIZE);  // buffer to hold incoming packet,
    char *ReadbackBuffer=malloc(UDP_PACKET_SIZE - 1);        // a string to send back
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(IncomingBuffer, packetSize);
    IncomingBuffer[BITS+1] = '\0';
    Serial.println("Contents:");
    Serial.println(IncomingBuffer);



    if (IncomingBuffer[0] == 's') {
      set_array(IncomingBuffer);
      char *returnBuffer = build_readback_array("write array");

      for (int i = 0; i < BITS; i++) {
        ReadbackBuffer[i] = returnBuffer[i];
      }
      ReadbackBuffer[BITS] = '\0';
      //https://stackoverflow.com/a/29655158/4692610 says I can do this
      free(returnBuffer);
    }
    else if (IncomingBuffer[0] == 'w') {
      write_chip(true);
      char *returnBuffer = build_readback_array("readback array");

      for (int i = 0; i < BITS; i++) {
        ReadbackBuffer[i] = returnBuffer[i];
      }
      ReadbackBuffer[BITS] = '\0';
      free(returnBuffer);
    }
    else {
      Serial.print("Error: Function indicator not correct.  Should be 's' or 'w'.  Was ");
      Serial.println(IncomingBuffer[0]);
      //todo put error message in Readback Buffer
    }

    Serial.println("Sending over UDP:");
    Serial.println(ReadbackBuffer);

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Remoteip, sendPort);
    Udp.write(ReadbackBuffer);
    Udp.endPacket();

    free(IncomingBuffer);
    free(ReadbackBuffer);
  }
  delay(10);
}

char* build_readback_array(String sourceArray) {
  char *returnBuffer = (char *) malloc(sizeof(char) * BITS);

  for (int i = 0; i < BITS; i++) {
    bool value;
    byte which_byte = i / 8;
    byte which_bit = i % 8;

    if (sourceArray == "write array"){
      value = bitRead(array_to_set[which_byte], which_bit);
    }
    else if (sourceArray == "readback array"){
      value = bitRead(array_readback[which_byte], which_bit);
    }
    else{
      Serial.print(F("Error: build_readback_array requested an array of "));
      Serial.println(sourceArray);
    }

    if (value) {
      returnBuffer[i] = '1';
    }
    else if (!value) {
      returnBuffer[i] = '0';
    }
    else {
      Serial.print(F("ERROR: Value "));
      Serial.print(i);
      Serial.print(F(" was "));
      Serial.println(value);
    }
  }
  returnBuffer[BITS] = '\0';
  Serial.println("Built return buffer:");
  Serial.println(returnBuffer);
  return returnBuffer;
}

void set_array(char incomingArray[]) {
  bool value;
  for (int i = 0; i < BITS; i++) {
    byte which_byte = i / 8;
    byte which_bit = i % 8;
    if (incomingArray[i + 1] == high) {
      value = true;
    }
    else if (incomingArray[i + 1] == low) {
      value = false;
    }
    else {
      Serial.print(F("ERROR: Character "));
      Serial.print(i);
      Serial.print(F(" was "));
      Serial.println(incomingArray[i]);
    }
    bitWrite(array_to_set[which_byte], which_bit, value);
  }

  if (to_print == true) {
    Serial.print(F("Low Voltage Amplifier Bias Array (binary) is: "));
    for (int i = 0; i < 12; i++) {
      if (i == 4) {
        Serial.print("\nHigh Voltage SiPM Bias Array (binary) is: ");
      }
      for (int k = 0; k < 8; k++) {
        Serial.print((array_to_set[i] >> k) & 1);
      }
    }
    Serial.print("\n");

    Serial.print(F("Low Voltage Amplifier Bias Array (hex) is: "));
    for (int i = 0; i < 12; i++) {
      if (i == 4) {
        Serial.print("\nHigh Voltage SiPM Bias Array (hex) is: ");
      }
      Serial.print(array_to_set[i] >> 4, HEX);
      Serial.print(array_to_set[i] & 0xF, HEX);
    }
    Serial.print("\n");
  }
}

void write_chip(bool to_print) {
  //Cycles through this twice.  The first time shifts the settings in the latches, and the second time, the previous settings are outputted, so you can read and compare
  //This cycles through the 96 bits in the correct order.  For each bit, it is "clocked" in, and the output bit is read.
  //The HV 5523 and HV 3418 latch in the data on different clock edges, so the transient analysis in complicated, especially with the LTC6820 between them, but this was found experimentally to work
  //Because of the register orientation, you want to put the last bit first
  //With the LTC6820, a change of LE will send that across to the slave module
  //With PHA = 0, POL = 0, a rising clock edge will sample D_IN and send that data to the slave module.  The slave module will adjust MOSI and then do a quick positive/negative clock pulse
  //and then sample the readback on MISO and send that back.
  //HOWEVER, the Master only adjusts its MISO (sampled as D_OUT here) AFTER the Arduino does the falling clock edge.  Thanks Linear for hiding that information in a figure.

  //Wake up the sleeping module
  digitalWrite(LE, LOW);
  digitalWrite(LE, HIGH);

  for (int m = 0; m < 2; m++) {
    digitalWrite(LE, LOW);
    for (int i = 11; i > -1; i--) {
      for (int k = 7; k > -1; k--) {
        boolean input = digitalRead(D_OUT);
        bitWrite(array_readback[i], k, input);

        bool output = (array_to_set[i] >> k) & 1;
        digitalWrite(D_IN, output);
        delay(SPI_delay);

        digitalWrite(CLK, HIGH);
        delay(SPI_delay);

        digitalWrite(CLK, LOW);
        delay(SPI_delay);



        if ((i == 0) and (k == 0)) {
          digitalWrite(LE, HIGH);
          delay(SPI_delay);
          digitalWrite(LE, LOW);
          delay(SPI_delay);
          digitalWrite(LE, HIGH);
          delay(SPI_delay);
          digitalWrite(D_IN, LOW);
        }
      }
    }
  }

  //Check to make sure the chip was written correctly
  bool received = true;

  for (int i = 0; i < 12; i++) {
    if (array_readback[i] != array_to_set[i]) {
      received = false;
      Serial.print(F("For subarray "));
      Serial.print(i);
      Serial.print(F(", we wanted to see "));
      Serial.print(array_to_set[i]);
      Serial.print(F(", but instead we saw "));
      Serial.println(array_readback[i]);
    }
  }
  //Want it to always print SPI failures with timestamp (for during long term test), but only print successes if requested

  if (to_print == true) {
    if (received == true) {
      Serial.println(F("SPI Write Successful!"));
    }
  }

  if (received == false) {
    Serial.println(millis());
    Serial.println(F("ERROR: SPI Write Unsuccessful"));
  }
}

//Configuration explanation
//The IO-1733-1 board is designed with the SPI chain below with shared clock/LE line:

//input -> HV5523 -> HV3418 -> readback

//If you look at the data sheets, you'll see that the HV3418 shifts new bits in on a CLK rising edge and latches it (enables the output) on a LE falling edge
//while the HV5523 shifts new bits in on a CLK falling edge and latches on a LE rising edge.  I know, less than optimal, but these were the only chips that fit what we needed and worked at cryo.

//We have 4 options for the POL and PHA pins of the LTC6820.
//The datasheet shows that the SCK pulse lasts for 100 ns or 1us depending on the SLOW pin.  Because we're going to be in cryo, I'd rather use SLOW to give us the margin if we need it, so we make sure the MOSI pin has settled.
//At room temp, the HV3418 requires the data line to be stable for ~30 ns before and after the clock, which must be >83 ns.  The output reflects the bit that you pushed 160 ns after the clock pulses.  The timing requirements are a little less
//strict for the HV5523, but still, because everything could completely change in cryo, it's better being safe.  For that reason POL=0, PHA=1 and POL=1, PHA=0 are out because I don't want a falling/rising SCK pulse.
//That pattern would possibly create a race condition by shifting the HV5523 and immediately shifting it into the HV3418.  Because we have no control over the SCK pulse width, I'd much rather have a rising/falling SCK pulse.

//Now, with those options, POL=1, PHA=1 is more tricky to deal with.  The CS (or LE in our case) pin initiating the write also triggers SCK to fall and then rise at the end when LE comes back up.
//This shifts bits into either chip when we don't have control over the SPI data line.  It also makes things annoying at the end, where we need both LE edges to get both chips to latch, but in this case, that would actually keep shifting bits in.
//It would be just plain annoying to deal with all that.

//With POL=0, PHA=0, there are no suprise SCK edges from CS/LE, so I can just pulse those as much as I want to latch both chips.  The only downside is that the readback is shifted by one bit.  Because the first rising edge of the SCK pulse train
//Causes the HV3418 to shift in the last bit sent from the HV5523, the HV3418 that outputs the readback is one bit "behind".  I make up for this by having the random SCK pulse after the LE goes low in the write_chip() function