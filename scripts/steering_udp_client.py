import socket
import struct
import argparse
import string
import binascii

VERSION = "1.0"

EXPECTED_RESP_LEN = 96
PORT = 32000
NETWORK_TIMEOUT_PERIOD = 5
MSG_FORMAT_STR = "<c24sI"
EMPTY_VALUES = "000000000000000000000000"

def make_msg(type, values):
    msg = bytes(type,"utf-8") + bytearray(values)
    crc = binascii.crc32(msg)
    msg = struct.pack(MSG_FORMAT_STR, bytes(type,"utf-8"), bytearray(values), crc)
    return msg

def is_hex(s):
     hex_digits = set(string.hexdigits)
     # if s is long, then it is faster to check against a set
     return all(c in hex_digits for c in s)

def read(args):
    try:
        msg = make_msg('R', EMPTY_VALUES.encode('utf-8'))
        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.settimeout(NETWORK_TIMEOUT_PERIOD)
        client.sendto(msg, (args.ip, PORT))

        # Get Response and decode
        resp = client.recvfrom(EXPECTED_RESP_LEN)                
        
        ctype, values, crc = struct.unpack(MSG_FORMAT_STR, resp[0])        
        
        if(crc != binascii.crc32(resp[0][0:25])):
            print("CRC mismatch!")

        if(ctype.decode('utf-8') != 'S'):
            print("Error response")
        
        print(values.decode('utf8'))      

    except socket.timeout:
        print("Timed out waiting for response")

    except KeyboardInterrupt:
        pass

def write(args):
    try:
        args.value = args.value.upper()

        if(len(args.value) != 24):
            print("Expected 24 hexadecimal characters! Received " + str(len(args.value)))
            exit(-1)

        if(not is_hex(args.value)):
            print("Must only contain [0-9,A-F]")
            exit(-1)

        msg = make_msg('W', args.value.encode('utf-8'))

        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.settimeout(NETWORK_TIMEOUT_PERIOD)
        client.sendto(msg, (args.ip, PORT))
        resp = client.recvfrom(EXPECTED_RESP_LEN)
        
        ctype, values, crc = struct.unpack(MSG_FORMAT_STR, resp[0])        
        
        if(crc != binascii.crc32(resp[0][0:25])):
            print("CRC mismatch!")

        if(ctype.decode('utf-8') != 'S'):
            print("Error response")
        
        print(values.decode('utf8'))    

    except socket.timeout:
        print("Timed out waiting for response")

    except KeyboardInterrupt:
        pass


def debug(args):
    try:
        args.value = args.value.upper()

        if(args.value == 'TRUE') or (args.value == 'ON'):
            values = '100000000000000000000000'
        else:
            values = '000000000000000000000000'

        msg = make_msg('D', values.encode('utf-8'))

        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.settimeout(NETWORK_TIMEOUT_PERIOD)
        client.sendto(msg, (args.ip, PORT))
        resp = client.recvfrom(EXPECTED_RESP_LEN)
        
        ctype, values, crc = struct.unpack(MSG_FORMAT_STR, resp[0])        
        
        if(crc != binascii.crc32(resp[0][0:25])):
            print("CRC mismatch!")

        if(ctype.decode('utf-8') != 'S'):
            print("Error response")
        
        print('Debug mode changed')    

    except socket.timeout:
        print("Timed out waiting for response")

    except KeyboardInterrupt:
        pass

def save(args):
    try:
        msg = make_msg('S', EMPTY_VALUES.encode('utf-8'))

        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.settimeout(NETWORK_TIMEOUT_PERIOD)
        client.sendto(msg, (args.ip, PORT))
        resp = client.recvfrom(EXPECTED_RESP_LEN)
        
        ctype, values, crc = struct.unpack(MSG_FORMAT_STR, resp[0])        
        
        if(crc != binascii.crc32(resp[0][0:25])):
            print("CRC mismatch!")

        if(ctype.decode('utf-8') != 'S'):
            print("Error response")
        
        print(values.decode('utf8'))    

    except socket.timeout:
        print("Timed out waiting for response")

    except KeyboardInterrupt:
        pass

def load(args):
    try:
        msg = make_msg('L', EMPTY_VALUES.encode('utf-8'))

        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.settimeout(NETWORK_TIMEOUT_PERIOD)
        client.sendto(msg, (args.ip, PORT))
        resp = client.recvfrom(EXPECTED_RESP_LEN)
        
        ctype, values, crc = struct.unpack(MSG_FORMAT_STR, resp[0])        
        
        if(crc != binascii.crc32(resp[0][0:25])):
            print("CRC mismatch!")

        if(ctype.decode('utf-8') != 'S'):
            print("Error response")
        
        print(values.decode('utf8'))    

    except socket.timeout:
        print("Timed out waiting for response")

    except KeyboardInterrupt:
        pass


def build(args):
    try:
        msg = make_msg('B', EMPTY_VALUES.encode('utf-8'))

        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.settimeout(NETWORK_TIMEOUT_PERIOD)
        client.sendto(msg, (args.ip, PORT))
        resp = client.recvfrom(EXPECTED_RESP_LEN)
        
        ctype, values, crc = struct.unpack(MSG_FORMAT_STR, resp[0])        
        
        if(crc != binascii.crc32(resp[0][0:25])):
            print("CRC mismatch!")

        if(ctype.decode('utf-8') != 'S'):
            print("Error response")
        
        print(values.decode('utf8'))    

    except socket.timeout:
        print("Timed out waiting for response")

    except KeyboardInterrupt:
        pass    

def main():
    prog='steering_udp_client'
    parser = argparse.ArgumentParser(prog=prog)
    parser.add_argument('--version', action='version', version='%(prog)s ' + VERSION)
    parser.add_argument("ip", help="IP address of steering module")

    subparsers = parser.add_subparsers(title='subcommands', description='valid subcommands', help='additional help')

    parser_write = subparsers.add_parser('write', aliases=['w','wr'], help='Write new values to chip')
    parser_write.add_argument('value', help='24 Hexadecimal values to write to chip (96 bits)')    
    parser_write.set_defaults(func=write)

    parser_read = subparsers.add_parser('read', aliases=['r','rd'],  help='Read current chip values')
    parser_read.set_defaults(func=read)

    parser_save = subparsers.add_parser('save', help='Save active values to EEPROM')
    parser_save.set_defaults(func=save)

    parser_load = subparsers.add_parser('load', help='Load values from EEPROM into chip')
    parser_load.set_defaults(func=load)

    parser_build = subparsers.add_parser('build', help='Get build timestamp')
    parser_build.set_defaults(func=build)

    parser_debug = subparsers.add_parser('debug', help='Set debug mode')
    parser_debug.add_argument('value', help='Enable/Disable debug messages in serial console')    
    parser_debug.set_defaults(func=debug)

    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()    