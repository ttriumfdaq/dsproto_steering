# Darkside Steering Module

This package contains both Arduino code and a midas interface for the Darkside steering module and control module. The steering module communicates with a PDU to turn on/off power to the 16 tiles. The control module does the same but for 16 PDUs.

## Arduino code

Various versions of the Arduino code are found in the steering-bnl, steering and control directories.

## Midas interface

There are separate midas interfaces for the steering module and control module. They are both found in the midas directory.

Run `python3 control_module_frontend.py` briefly, then stop it. This is just to set up the relevant area in the ODB.

Edit the connection settings in the midas ODB at `/Equipment/ControlModule/Settings`. The important keys are `Use USB` (to choose USB or ethernet) and then either `USB port` or `IP address`.

Add a key to the midas ODB `/Custom/Control module` that is a string. Set the value to `/path/to/dsproto_steering/midas/control_module.html`. The midas webpages will now contain a link to the web interface for the control module.

Start the frontend again `python3 control_module_frontend.py`. Use the webpage to enable/disable PDUs and channels. The commands should be sent to the PDUs and the channel status updated (red=off, green=on).


## Documentation of the original steering module code

### Installation

Run `make docker` to build the docker image required

The user must be within the docker group in order to run `make upload` or `make terminal`, as these tasks require access to the USB

### Makefile Commands

- help
- docker
- interactive
- build
- terminal
- upload

### Usage

Run `make docker` to build the docker image used for the project

Run `make interactive` to interactively use the docker image

Run `make build` to create the binaries

Run `make upload` to upload the binaries to the device, the PORT may be specified if not `/dev/ttyUSB0`. Ie: `make upload PORT=/dev/ttyUSB1`

Run `make terminal` to connect to the device and see the log messages. The PORT may be specified if not `/dev/ttyUSB0`. Ie: `make terminal PORT=/dev/ttyUSB1`

### Steering Client

#### Commands

W - Write new values into chip
R - Read values chip has
S - Save 'set' values to EEPROM
L - Load 'set' values from EEPROM and write them to chip
B - Get build timestamp
D - Sets debug print messages on/off

#### PROTOCOL

UDP
PORT 32000

#### Message Format

29 bits

TYPE (1) 
VALUES (24)
CRC32 (4)

#### RESPONSE 

EnnnnnnnnnnnnnnnnnnnnnnnnCCCC

E = Error
n = 24 ascii values
C = CRC32

#### CRC

CRC32 is generated treating TYPE and VALUES as a single byte array, zlib crc32 (default for Arduino CRC32 module and python binascii)