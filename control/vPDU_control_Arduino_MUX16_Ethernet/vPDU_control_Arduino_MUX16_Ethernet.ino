//Eric Raguzin, Brookhaven National Laboratory
//Martin Spangenberg, University of Warwick
//
//DarkSide vPDU steering module control with LTC6820 interface
//
#include <Wire.h>
#include <WiFi.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <CRC32.h>
//Datasheets available here: 
//http://ww1.microchip.com/downloads/en/DeviceDoc/hv5523.pdf
//http://ww1.microchip.com/downloads/en/DeviceDoc/20005843A.pdf
//https://www.analog.com/media/en/technical-documentation/data-sheets/6820fb.pdf

//LTC6820 must be in the POL=0, PHA=1 positions.  Full explanation at bottom of file


////////////////////////////////////////////////////////////////////////////
// User configuration
////////////////////////////////////////////////////////////////////////////
#define USE_DHCP 1 // comment this out to use a static IP
#define DHCP_RETRY_DELAY_IN_MS 2000

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
uint8_t mac_address[6];
IPAddress ip(192, 168, 1, 200);

// Which pinout to use for multiplexer.
//#define USE_FEATHER_PINS 1 // Feather 328P
#define USE_HUZZAH_PINS 1  // Huzzah32

//Adjust speed of SPI write (to compensate for any issues in cryo)/////////
#define SPI_delay 0

////////////////////////////////////////////////////////////////////////////
// Shouldn't need to edit anything below here for your specific board/site/
////////////////////////////////////////////////////////////////////////////

// To translate to LTC6820 language, MOSI is D_IN and DATA, MISO is D_OUT and READBACK
// Choose your board here (or add another one if needed)

#ifdef USE_FEATHER_PINS
// SPI pins for multiplexer - Adafruit ESP32
#define D_IN  11  //MOSI Data out
#define D_OUT 12  //MISO Readback coming in
#define LE    5
#define CLK   13

// Control pins for multiplexer - Adafruit ESP32
#define mux_A0  7
#define mux_A1  6
#define mux_EN3 9
#define mux_EN2 2
#define mux_EN1 3
#define mux_EN0 4

#define ETHERNET_CS_PIN 10
#endif


#ifdef USE_HUZZAH_PINS
// SPI pins for multiplexer - Huzzah ESP32
#define D_IN  3  //MOSI Data out
#define D_OUT 2  //MISO Readback coming in
#define LE    5
#define CLK   4

// Control pins for multiplexer - Huzzah ESP32
#define mux_A0  7
#define mux_A1  8
#define mux_EN3  9
#define mux_EN2  10
#define mux_EN1  11
#define mux_EN0  12

#define ETHERNET_CS_PIN 33
#endif

#define BITS 96
#define PACKED_BITS (BITS/4) // packed bits into hexadecimal characters, 24 * 4 = 96
#define CMD_SIZE (1)
#define CRC_SIZE (4)
#define EEPROM_SIZE (PACKED_BITS + CRC_SIZE)
#define UDP_PACKET_SIZE (CMD_SIZE + PACKED_BITS + CRC_SIZE) // TYPE(CHAR) + PACKED BITS + CRC32 (uint32_t)

#define PORT 32000      // local port to listen on
  
// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

bool debug = true;
bool ethernet_alive = false;
bool ethernet_connected = false;

//I use a char array for the 64+32 bit stream instead of a 64 bit int or something because of this : https://forum.arduino.cc/index.php?topic=496719.0
//It's 12 arrays of 8 bits each, so it's a little more complicated to access, but is essentially the same.
//It's organized so that the first 4 arrays are for the low voltage amplifier bias, the last 8 are for the high voltage SiPM bias
//When printed, the arrays go from Channel 1 on the left to Channel 32 or 64 on the right
//This defaults to all outputs NOT allowing high voltage (the two chips require opposite polarities for that).
//All off
byte array_to_set[12] = {255,255,255,255,0,0,0,0,0,0,0,0};

byte array_readback[12] = {0};

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data
boolean newData = false;

struct argument {
   byte pin;
   bool value;
};

void setup() {
  pinMode(D_IN, OUTPUT);
  pinMode(D_OUT, INPUT);
  pinMode(LE, OUTPUT);
  pinMode(CLK, OUTPUT);

  pinMode(mux_A0,OUTPUT);
  pinMode(mux_A1,OUTPUT);
  pinMode(mux_EN3,OUTPUT);
  pinMode(mux_EN2,OUTPUT);
  pinMode(mux_EN1,OUTPUT);
  pinMode(mux_EN0,OUTPUT);

  muxEnable(0); // Start with multiplexer output 0 active

  //Latch off
  //POL 0, PHA 0 requires CLK to idle low
  digitalWrite(LE, HIGH);
  digitalWrite(CLK, LOW);
  digitalWrite(D_IN, LOW);

  //Writes the initial array to the chips so that all outputs are off.  It has a habit of starting up in a state that allows HV through.
  write_chip(false);
  
  Serial.begin (9600);
  delay(100); //It helps with the weird characters you get at the beginning
  Serial.println(F("DarkSide vPDU steering module controller"));
  Serial.print(F("The available commands are:\n'lv': This sets one channel of the low voltage amplifier bias array of 32 bits, but does not yet write it to the chip.  For example, to prepare pin 15 to be 1, "));
  Serial.print(F("use the syntax 'lv 15 1'.  To prepare pin 29 to be 0, use 'lv 29 0'.\n'hv': This sets one channel of the high voltage SiPM bias array of 64 bits, but does not yet write it to the chip.  "));
  Serial.print(F("For example, to prepare pin 5 to be 1, use the syntax 'hv 5 1'.  To prepare pin 16 to be 0, use 'hv 16 0'.\n"));
  Serial.print(F("'tile': This automatically enables the correct lv and hv bits to fully enable a particular tile. For example, to enable both lv and hv on tile 5, use the syntax 'tile 5 1'. To disable both lv and hv on tile 10, use the syntax 'tile 10 0'.\n"));
  Serial.print(F("'on': Enables lv/hv on all tiles.\n"));
  Serial.print(F("'off': Disables lv/hv on all tiles.\n"));
  Serial.print(F("'write': This will write the prepared array to the HV3418 chip.  At the end of the write sequence, you will receive either a 'SPI Write Successful' or an error message.\n"));
  Serial.print(F("'print': This prints the full lv and hv arrays in binary format. Remember to 'write' the arrays to the steering module, or else they might not correspond to the state of the vPDU.\n"));
  Serial.print(F("NOTE: With the low voltage chip, a 0 means that channel will be at the control voltage, and a 1 will mean that channel will be at ground potential.\n"));
  Serial.print(F("NOTE: With the high voltage chip, a 0 means that channel is at ground potential and a 1 means that channel will be at the high voltage.\n"));
  Serial.print(F("NOTE: When each array is printed, they display from Channel 0 on the left to Channel 32 or 64 on the right\n\n"));

  Serial.println(F("Initial settings:"));
  Serial.print(F("Low Voltage Amplifier Bias Array (hex) is: "));
  for (int i = 0; i < 12; i++){
    if (i == 4){
      Serial.print(F("\nHigh Voltage SiPM Bias Array (hex) is: "));
    }
    Serial.print(array_to_set[i] >> 4, HEX);
    Serial.print(array_to_set[i] & 0xF, HEX);
  }
  Serial.print(F("\n\n"));
  
  WiFi.macAddress(mac_address);
  Serial.print(F("MAC address: "));
  for (byte octet = 0; octet < 6; octet++) {
    Serial.print(mac_address[octet], HEX);
    if (octet < 5) {
      Serial.print(F(":"));
    }
  }
  Serial.println();

  // You can use Ethernet.init(pin) to configure the CS pin
  Ethernet.init(ETHERNET_CS_PIN);  // Most Arduino shields

  // start the Ethernet
  if(USE_DHCP) {  
    ethernet_alive = Ethernet.begin(mac_address);
  } else {
    Ethernet.begin(mac_address, ip);
    ethernet_alive = true;
  }
  
  // Check for Ethernet hardware present
  if(Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println(F("Ethernet shield was not found. Running without ethernet!"));
  }

  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println(F("Ethernet cable is not connected."));
  } else {
    ethernet_connected = true;
  }

  if(ethernet_alive) {
    Serial.print(F("Listening on "));
    Serial.print(Ethernet.localIP());
    Serial.print(F(":"));
    Serial.println(PORT);
  } else {
    Serial.println(F("Failed to get IP address"));
  }

  // start UDP
  Udp.begin(PORT);
}

//The main loop basically checks for new Serial data, and outputs a flag if it receives a new line.  Then that flag tells the program to parse it.  This seems to be the most reliable way to recieve strings of variable length
void loop() {
  recvWithEndMarker();
  if (newData == true) {
    newData = false;
    parse_input();
  }

  parse_ethernet();
}

//As long as there's data in the Serial buffer, it reads it one char at a time, and fills in the char array.  Once it sees the end marker, it finishes up and gives a null termination.
//The Arduino terminal needs to be told to give that end marker whenever you send a command.
void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
  while (Serial.available() > 0 && newData == false) {
    rc = Serial.read();
    
    if (rc != endMarker) {
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars) {
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0'; // terminate the string
      ndx = 0;
      newData = true;
    }
  }
}

void parse_input(){
  struct argument b;
  
  char compare[15];
  int ret_hv;
  int ret_lv;
  int ret_write;
  int ret_test;
  int ret_on;
  int ret_off;
  int ret_mux;
  int ret_tile;
  int ret_print;

  const char s[2] = " ";
  char *command_token;
  char *reg_token;
  char *val_token;
  
  //get the token separated by each space character
  command_token = strtok(receivedChars, s);
  reg_token = strtok(NULL, s);
  val_token = strtok(NULL, s);

  //I think this is the simplest way to compare a string token and see if it matches any preset strings

  strcpy(compare, "hv");
  ret_hv = strcmp(compare, command_token);

  strcpy(compare, "lv");
  ret_lv = strcmp(compare, command_token);

  strcpy(compare, "write");
  ret_write = strcmp(compare, command_token);

  strcpy(compare, "test");
  ret_test = strcmp(compare, command_token);

  strcpy(compare, "on");
  ret_on = strcmp(compare, command_token);

  strcpy(compare, "off");
  ret_off = strcmp(compare, command_token);

  strcpy(compare, "mux");
  ret_mux = strcmp(compare, command_token);

  strcpy(compare, "tile");
  ret_tile = strcmp(compare, command_token);

  strcpy(compare, "print");
  ret_print = strcmp(compare, command_token);
  if(ret_hv == 0){
    if(reg_token){
      int reg = atoi(reg_token);
      int val = atoi(val_token);
      set_hv(reg, val);
    }
    else{
      Serial.println(F("ERROR: You typed 'hv' but no register number was found.  The syntax needs to be 'hv reg# value#'."));
    }
  }
  else if(ret_lv == 0){
    if(reg_token){
      int reg = atoi(reg_token);
      int val = atoi(val_token);
      set_lv(reg, val);
    }
    else{
      Serial.println(F("ERROR: You typed 'lv' but no register number was found.  The syntax needs to be 'hv reg# value#'."));
    }
  }
  else if (ret_write == 0){
    write_chip(true);
    }
  else if (ret_test == 0){
    //write_chip(true);
    bool result = switch_test();
    if (result == true){
      Serial.println(F("Test was successful!"));
    }
    else{
      Serial.println(F("Test was not successful"));
    }
  }
  else if (ret_mux == 0){
    int chan = atoi(reg_token);
    muxEnable(chan);
    Serial.print(F("Enabling mux channel "));
    Serial.println(chan);
  }
  else if (ret_tile == 0){
    int itile = atoi(reg_token);
    int value = atoi(val_token);

    if ((reg_token and val_token) and (value == 0 or value == 1) and (itile > 0 and itile < 17)){
      set_hv(itile, value);
  
      int val_lv = (value==1) ? 0 : 1;
  
      int bit_fe = 11 + 5*(int(itile-1) / 4); // The lv bit corresponding to the frontend electronics
      int bit_tile = bit_fe + (itile-1) % 4 + 1; // The lv bit corresponding to the tile

      set_lv(bit_tile, val_lv);

      // Get full lv bit array
      int lv_bits[32];
      for (int i = 0; i < 4; i++){
        for (int k = 0; k < 8; k++){
         lv_bits[i*8+k] = (array_to_set[i] >> k) & 1;
        }
      }
      
      if(value == 1){
        set_lv(bit_fe, val_lv);
      }
      //Check if any tiles in a quadrant are still on. If not, turn off power to quadrant frontend electronics
      else if(!(lv_bits[bit_fe] == 0 or lv_bits[bit_fe+1] == 0 or lv_bits[bit_fe+2] == 0 or lv_bits[bit_fe+3] == 0)){
        set_lv(bit_fe, val_lv);
      }

      if (value == 1){
        Serial.print(F("LV and HV have been enabled for tile "));
      }
      else{
        Serial.print(F("LV and HV have been disabled for tile "));
      }
      Serial.println(itile);
    }
    else{
      Serial.println(F("Error. Tile number must be between 1 and 16, and value must be 0 or 1"));
    }
  }
  else if (ret_off == 0){
    for (int ibyte = 0; ibyte < 4; ibyte++){
      array_to_set[ibyte] = 255;
    }
    for (int ibyte = 4; ibyte < 12; ibyte++){
      array_to_set[ibyte] = 0;
    }
    Serial.println(F("LV/HV have been turned OFF for all tiles"));
  }
  else if (ret_on == 0){
    for (int ibyte = 0; ibyte < 4; ibyte++){
      array_to_set[ibyte] = 0;
    }
    for (int ibyte = 4; ibyte < 12; ibyte++){
      array_to_set[ibyte] = 255;
    }
    Serial.println(F("LV/HV have been turned ON for all tiles"));
  }
  else if (ret_print == 0){
    Serial.print(F("Low Voltage Amplifier Bias Array (binary) is: "));
    for (int i = 0; i < 12; i++){
      if (i == 4){
        Serial.print(F("\nHigh Voltage SiPM Bias Array (binary) is: "));
      }
      for (int k = 0; k < 8; k++){
        Serial.print((array_to_set[i] >> k) & 1);
      }
    }
  }
  else{
    Serial.print(F("Incorrect syntax, no function detected!  Use the 'hv','lv','write','test','on' or 'off' command with proper arguments.  You typed: "));
    Serial.println(receivedChars);
  }
  Serial.print(F("\n"));
}

void set_hv(int ipin, int value){
  struct argument b;
  if (ipin < 1 || ipin > 64) {
    Serial.println(F("ERROR: You typed 'hv' but the register is out of range.  For high voltage it should be between 1 and 64.  You typed:"));
    Serial.println(ipin);
  }
  else if(value == 0 or value == 1){
    //Because we want to subtract it by 1 to zero index, but also add 32 to offset it to the right part of the array
    b.pin = (ipin+31);
    b.value = value;
    set_array(b, false);
    Serial.print(F("Setting HV pin "));
    Serial.print(ipin);
    Serial.print(F(" to "));
    Serial.println(value);
  }
  else{
    Serial.println(F("ERROR: You typed 'hv' but no value was found.  The syntax needs to be 'set reg# value#'.  value# is a 1 or 0."));
  }
}

void set_lv(int ipin, int value){
  struct argument b;
  if (ipin < 1 || ipin > 32) {
    Serial.println(F("ERROR: You typed 'lv', but the register is out of range.  For low voltage it should be between 1 and 32.  You typed:"));
    Serial.println(ipin);
  }
  else if(value == 0 or value == 1){
    //To zero index
    b.pin = (ipin-1);
    b.value = value;
    set_array(b, false);
    Serial.print(F("Setting LV pin "));
    Serial.print(ipin);
    Serial.print(F(" to "));
    Serial.println(value);
  }
  else{
    Serial.println(F("ERROR: You typed 'lv' but no value was found.  The syntax needs to be 'set reg# value#'.  value# is a 1 or 0."));
  }
}

void set_array(argument b, bool to_print){
  if (b.pin < 96 and b.pin > -1 and (b.value == 0 or b.value == 1)){
    if (to_print == true){
      if (b.pin > 31){
        Serial.print(F("Changing HV pin "));
        //Bring it back to user notation
        Serial.print(b.pin-31);
        Serial.print(F(" to "));
        Serial.println(b.value);
      }
      else{
        Serial.print(F("Changing LV pin "));
        //Bring it back to user notation
        Serial.print(b.pin+1);
        Serial.print(F(" to "));
        Serial.println(b.value);
      }
    }

      byte which_byte = b.pin/8;
      byte which_bit = b.pin%8;
      
      bitWrite(array_to_set[which_byte], which_bit, b.value);

    if (to_print == true){
      Serial.print(F("Low Voltage Amplifier Bias Array (binary) is: "));
      for (int i = 0; i < 12; i++){
        if (i == 4){
          Serial.print(F("\nHigh Voltage SiPM Bias Array (binary) is: "));
        }
        for (int k = 0; k < 8; k++){
          Serial.print((array_to_set[i] >> k) & 1);
        }
      }
      Serial.print(F("\n"));

      Serial.print(F("Low Voltage Amplifier Bias Array (hex) is: "));
      for (int i = 0; i < 12; i++){
        if (i == 4){
          Serial.print(F("\nHigh Voltage SiPM Bias Array (hex) is: "));
        }
        Serial.print(array_to_set[i] >> 4, HEX);
        Serial.print(array_to_set[i] & 0xF, HEX);
      }
      Serial.print(F("\n"));
    }
  }
  else{
    Serial.print(F("ERROR: Trying to change pin "));
    //Bring it back to user notation
    Serial.print(b.pin+1);
    Serial.print(F(" to "));
    Serial.println(b.value);
  }
}

void write_chip(bool to_print){
  //Cycles through this twice.  The first time shifts the settings in the latches, and the second time, the previous settings are outputted, so you can read and compare
  //This cycles through the 96 bits in the correct order.  For each bit, it is "clocked" in, and the output bit is read.
  //The HV 5523 and HV 3418 latch in the data on different clock edges, so the transient analysis in complicated, especially with the LTC6820 between them, but this was found experimentally to work
  //Because of the register orientation, you want to put the last bit first
  //With the LTC6820, a change of LE will send that across to the slave module
  //With PHA = 0, POL = 0, a rising clock edge will sample D_IN and send that data to the slave module.  The slave module will adjust MOSI and then do a quick positive/negative clock pulse
  //and then sample the readback on MISO and send that back.
  //HOWEVER, the Master only adjusts its MISO (sampled as D_OUT here) AFTER the Arduino does the falling clock edge.  Thanks Linear for hiding that information in a figure.

  //Wake up the sleeping module
  digitalWrite(LE, LOW);
  digitalWrite(LE, HIGH);
  
  for (int m = 0; m < 2; m++){
    digitalWrite(LE, LOW);
    

    
    for (int i = 11; i > -1; i--){
      for (int k = 7; k > -1; k--){
        bool output = (array_to_set[i] >> k) & 1;
        digitalWrite(D_IN, output);
        delay(SPI_delay);

        boolean input = digitalRead(D_OUT);
        bitWrite(array_readback[i],k,input);
        
        digitalWrite(CLK, HIGH);
        delay(SPI_delay);
        
        digitalWrite(CLK, LOW);
        delay(SPI_delay);
        
        
        
        if ((i == 0) and (k == 0)){
          
          digitalWrite(LE, HIGH);
          delay(SPI_delay);
          digitalWrite(LE, LOW);
          delay(SPI_delay);
          digitalWrite(LE, HIGH);
          delay(SPI_delay);
          digitalWrite(D_IN, LOW);
        }
      }
    }
    
  }

  //Check to make sure the chip was written correctly
  bool received = true;

  for (int i = 0; i < 12; i++){
    if (array_readback[i] != array_to_set[i]){
      received = false;
      Serial.print(F("For subarray "));
      Serial.print(i);
      Serial.print(F(", we wanted to see "));
      Serial.print(array_to_set[i]);
      Serial.print(F(", but instead we saw "));
      Serial.println(array_readback[i]);
    }
  }
  //Want it to always print SPI failures with timestamp (for during long term test), but only print successes if requested
  
  if (to_print == true){
    if (received == true){
      Serial.println(F("SPI Write Successful!"));
    }
  }
  
  if (received == false){
    Serial.println(millis());
    Serial.println(F("ERROR: SPI Write Unsuccessful"));
    }
}

bool switch_test(){
  for (int i = 0; i < 100; i++){
    delay(1000);
    digitalWrite(LE, LOW);
    //delay(1);
    digitalWrite(D_IN, LOW);
    digitalWrite(CLK, LOW);
    //delay(1);
    digitalWrite(CLK, HIGH);
    digitalWrite(D_IN, HIGH);
    //delay(1);
    digitalWrite(CLK, LOW);
    //delay(1);
    digitalWrite(CLK, HIGH);
    digitalWrite(D_IN, LOW);
    //delay(1);
    digitalWrite(CLK, LOW);
    //delay(1);
    digitalWrite(CLK, HIGH);
    digitalWrite(D_IN, HIGH);
    //delay(1);
    //digitalWrite(LE, HIGH);
  }
  return true;
}


void muxEnable(int channel)
{
  //Note:
  //RJ45 Port 0 = left hand side
  //RJ45 Port 3 = right hand side
  
  
  switch(channel)
  {
    
    //RJ45 Port 0
    case 0: setMuxPins(1,1,0,0,0,1); break;   //pin 1,2  
    case 1: setMuxPins(1,0,0,0,0,1); break;    //pin 3,6 
    case 2: setMuxPins(0,1,0,0,0,1); break;    //pin 4,5 
    case 3: setMuxPins(0,0,0,0,0,1); break;    //pin 7,8 
    
    //RJ45 Port 1
    case 4: setMuxPins(1,1,0,0,1,0); break;    //pin 1,2 
    case 5: setMuxPins(1,0,0,0,1,0); break;    //pin 3,6 
    case 6: setMuxPins(0,1,0,0,1,0); break;    //pin 4,5 
    case 7: setMuxPins(0,0,0,0,1,0); break;    //pin 7,8 
    
    //RJ45 Port 2
    case 8: setMuxPins(1,1,0,1,0,0); break;    //pin 1,2 
    case 9: setMuxPins(1,0,0,1,0,0); break;    //pin 3,6 
    case 10: setMuxPins(0,1,0,1,0,0); break;   //pin 4,5 
    case 11: setMuxPins(0,0,0,1,0,0); break;   //pin 7,8 
    
    //RJ45 Port 3
    case 12: setMuxPins(1,1,1,0,0,0); break;   //pin 1,2 
    case 13: setMuxPins(1,0,1,0,0,0); break;   //pin 3,6 
    case 14: setMuxPins(0,1,1,0,0,0); break;   //pin 4,5 
    case 15: setMuxPins(0,0,1,0,0,0); break;   //pin 7,8 
  }
}


void setMuxPins(int a1, int a0, int e3, int e2, int e1, int e0)
{
  digitalWrite(mux_A0,a0);
  digitalWrite(mux_A1,a1);
  digitalWrite(mux_EN3,e3);
  digitalWrite(mux_EN2,e2);
  digitalWrite(mux_EN1,e1);
  digitalWrite(mux_EN0,e0);
}

void parse_ethernet() {
  static char rxBuffer[UDP_PACKET_SIZE+1];
  static char txBuffer[UDP_PACKET_SIZE+1];
  uint16_t bytesToSend;

  if((ethernet_connected) && (Ethernet.linkStatus() == LinkOFF)) {
    Serial.println(F("Ethernet cable is not connected."));
    ethernet_connected = false;
  } 
  
  if((!ethernet_connected) && (Ethernet.linkStatus() == LinkON)) {
    Serial.println(F("Ethernet cable is connected."));
    ethernet_connected = true;
  }

  if(USE_DHCP) {
    byte result = Ethernet.maintain();
    if((!ethernet_alive) && ((result == 2) || (result == 4))) {
      ethernet_alive = true;
      if(ethernet_alive) {
        Serial.print(F("Listening on "));
        Serial.print(Ethernet.localIP());
        Serial.print(F(":"));
        Serial.println(PORT);
      }
    }
  }

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();

  if (packetSize == UDP_PACKET_SIZE) {
    
    // Get IP and PORT
    IPAddress remote_ip = Udp.remoteIP();
    uint16_t remote_port = Udp.remotePort();

    packetSize = Udp.read(rxBuffer, UDP_PACKET_SIZE);
    rxBuffer[packetSize] = '\0'; // safe because rxBuffer is UDP_PACKET_SIZE+1, and packetSize is max UDP_PACKET_SIZE
    if(debug) {
      Serial.print(F("Received packet of size "));
      Serial.print(packetSize);
      Serial.print(F(" from "));
      Serial.print(remote_ip);
      Serial.print(F(":"));
      Serial.println(remote_port);
      Serial.print(F("RX: "));
      Serial.println(rxBuffer);
    }

    // Validate CRC32 and fail silently if it doesn't work ( security thru obscurity :) )
    if(*((uint32_t*)&rxBuffer[PACKED_BITS+1]) != CRC32::calculate(rxBuffer, PACKED_BITS+1)) {
      if(debug) {
        Serial.println(F("Invalid CRC32 received"));
        Serial.println(*(uint32_t*)&rxBuffer[PACKED_BITS+1]);
        Serial.println(CRC32::calculate(rxBuffer, PACKED_BITS+1));
      }
      return;
    }

    // default to error message
    txBuffer[0] = 'E';
    for(int i=0; i<PACKED_BITS; i++) {
      txBuffer[i+1] = '0';
    }   

    const char space[2] = " ";
    char *command;
    char* chan;

    switch(rxBuffer[0]) {
      case 'M': // switch mux 

        //get the token separated by each space character
        command = strtok(rxBuffer, space);
        chan = strtok(NULL, space);

        if(debug) {
          Serial.print(F("Enabling mux channel "));
          Serial.println(chan);
        }

        muxEnable(atoi(chan));
        txBuffer[0] = 'S';
        break;

      case 'W': // write               
        // Message too small to be valid
        if(packetSize != UDP_PACKET_SIZE) {          
          break;
        }

        // Read in chip values, and convert them from ascii '0','1' to 0,1
        if(convert_udp_msg_to_array(&rxBuffer[1], array_to_set)) {
          write_chip(false);
          convert_array_to_udp_msg(array_readback, &txBuffer[1]);
          txBuffer[0] = 'S';
        } else if(debug) {
          Serial.print(F("Error: Invalid array sent"));          
        }
        break;

      case 'R': // readback
        // rewrite the last given values to the chip and get the response
        write_chip(false);        
        convert_array_to_udp_msg(array_readback, &txBuffer[1]);
        txBuffer[0] = 'S';
        break;

      case 'D': // debug change
        debug = (rxBuffer[1] == '1') ? true : false;
        txBuffer[0] = 'S';
        break;

      case 'B': // build info
        sprintf(txBuffer, "%c%s", 'S', __TIMESTAMP__);
        break;

      default: // error
        if(debug) {
          Serial.print(F("Error: Function indicator not correct. Was "));
          Serial.println(rxBuffer[0]);
        }
        break;
    }

    *(uint32_t*)&txBuffer[PACKED_BITS+1] = CRC32::calculate(txBuffer, PACKED_BITS+1); 
    bytesToSend = UDP_PACKET_SIZE; // ERROR type + PACKED BITS + CRC32

    if(debug) {
      txBuffer[bytesToSend] = '\0'; // bytesToSend shall never exceed UDP_PACKET_SIZE, txBuffer is defined as UDP_PACKET_SIZE+1
      Serial.print(F("TX: "));
      Serial.println(txBuffer);
      Serial.print(F("Transmitting response to "));
      Serial.print(remote_ip);
      Serial.print(F(":"));
      Serial.println(remote_port);
    }

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(remote_ip, remote_port);
    Udp.write(txBuffer, bytesToSend);
    Udp.endPacket();  
  }
}

bool convert_udp_msg_to_array(const char* msg, byte* array) {
  uint8_t lo;
  uint8_t hi;

  // Validate the message before we write to the array
  for (int i = 0; i < PACKED_BITS; i++) {    
    if(!((msg[i] >= '0') && (msg[i] <= '9') || (msg[i] >= 'A') && (msg[i] <= 'F'))) {
      return false;
    }
  }

  // Set the array based on the message
  for (int i = 0; i < PACKED_BITS; i+=2) {
    if((msg[i] >= '0') && (msg[i] <= '9')) {
      lo = msg[i] - '0';
    } else {
      lo = (msg[i] - 'A') + 10;
    }
    if((msg[i+1] >= '0') && (msg[i+1] <= '9')) {
      hi = msg[i+1] - '0';
    } else {
      hi = (msg[i+1] - 'A') + 10;
    }

    array[i/2] = (hi<<4) | lo; 
  }

  return true;
}
     
void convert_array_to_udp_msg(const byte* array, char* msg) {
  uint8_t lo;
  uint8_t hi;

  for (int i = 0; i < PACKED_BITS; i+=2) {
    lo = array[i/2] & 0x0F;
    hi = ((array[i/2] & 0xF0) >> 4);

    msg[i] = (lo < 10) ? '0' + lo : 'A' + (lo - 10);
    msg[i+1] = (hi < 10) ? '0' + hi : 'A' + (hi - 10);
  }
}

//Configuration explanation
//The IO-1733-1 board is designed with the SPI chain below with shared clock/LE line:

//input -> HV5523 -> HV3418 -> readback

//If you look at the data sheets, you'll see that the HV3418 shifts new bits in on a CLK rising edge and latches it (enables the output) on a LE falling edge
//while the HV5523 shifts new bits in on a CLK falling edge and latches on a LE rising edge.  I know, less than optimal, but these were the only chips that fit what we needed and worked at cryo.

//We have 4 options for the POL and PHA pins of the LTC6820.  
//The datasheet shows that the SCK pulse lasts for 100 ns or 1us depending on the SLOW pin.  Because we're going to be in cryo, I'd rather use SLOW to give us the margin if we need it, so we make sure the MOSI pin has settled.
//At room temp, the HV3418 requires the data line to be stable for ~30 ns before and after the clock, which must be >83 ns.  The output reflects the bit that you pushed 160 ns after the clock pulses.  The timing requirements are a little less
//strict for the HV5523, but still, because everything could completely change in cryo, it's better being safe.  For that reason POL=0, PHA=1 and POL=1, PHA=0 are out because I don't want a falling/rising SCK pulse. 
//That pattern would possibly create a race condition by shifting the HV5523 and immediately shifting it into the HV3418.  Because we have no control over the SCK pulse width, I'd much rather have a rising/falling SCK pulse.

//Now, with those options, POL=1, PHA=1 is more tricky to deal with.  The CS (or LE in our case) pin initiating the write also triggers SCK to fall and then rise at the end when LE comes back up.
//This shifts bits into either chip when we don't have control over the SPI data line.  It also makes things annoying at the end, where we need both LE edges to get both chips to latch, but in this case, that would actually keep shifting bits in.
//It would be just plain annoying to deal with all that.

//With POL=0, PHA=0, there are no suprise SCK edges from CS/LE, so I can just pulse those as much as I want to latch both chips.  The only downside is that the readback is shifted by one bit.  Because the first rising edge of the SCK pulse train
//Causes the HV3418 to shift in the last bit sent from the HV5523, the HV3418 that outputs the readback is one bit "behind".  I make up for this by having the random SCK pulse after the LE goes low in the write_chip() function
